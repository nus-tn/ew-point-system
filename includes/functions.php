<?php
	require_once("constants.inc");
	require_once("db.php");

	/**
	* alert simulate javascript alert function
	**/
	function alert($msg) {
		echo '<script language="javascript">';
		echo 'alert("' . $msg .'")';
		echo '</script>';
	}
	/**
	 * 
	 */
	function user_store($matric) {
		$uid = get_user_id_by_matric_no($matric);
		if ($uid != -1) {
			$_SESSION['uid'] = $uid;
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function role_store($role) {
		$_SESSION['role'] = $role;
	}

	function user_log_out() {
		unset($_SESSION['uid']);
		unset($_SESSION['role']);
	}

	/**
	 * current_user: get information of the use who is currently logged in
	 * @return uid
	 */
	function current_user() {
		if (isset($_SESSION['uid'])) {
			return $_SESSION['uid'];
		} else {
			return FALSE;
		}
	}

	function current_role() {
		if (isset($_SESSION['role'])) {
			return $_SESSION['role'];
		} else {
			return FALSE;
		}
	}

	/**
	 * is_head_of_project: check whether the user is the wing/msw head for the project
	 * @param uid
	 * @param pid
	 * @return boolean
	 */
	function is_head_of_project($uid, $pid) {
		if (!is_head($uid)) {
			return FALSE;
		}

		if (get_project_wing($pid) != WING_NULL) {
			if (get_project_wing($pid) == get_user_wing($uid)) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else if (get_project_msw($pid) != MSW_NULL) {
			if (get_project_msw($pid) == MSW_CROSS) {
				return TRUE;
			} else if (get_project_msw($pid) == get_user_msw($uid)) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	/**
	 * is_head_of_member: check whether the user is the wing/msw head for the user
	 * @param uid1
	 * @param uid2
	 * @return boolean
	 */
	function is_head_of_member($uid1, $uid2) {
		if (!is_head($uid1)) {
			return FALSE;
		}

		if ((get_user_wing($uid2) != WING_NULL) && (get_user_msw($uid2) != MSW_NULL)) {
			return ((get_user_wing($uid1) == get_user_wing($uid2)) || (get_user_msw($uid1) == get_user_msw($uid2)));
		} else if (get_user_wing($uid2) != WING_NULL) {
			if (get_user_wing($uid1) == get_user_wing($uid2)) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else if (get_user_msw($uid2) != MSW_NULL) {
			if (get_user_msw($uid1) == get_user_msw($uid2)) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	/**
	 * get_user_total_point description
	 * @param $uid
	 * @return float
	 */
	function get_user_total_point($uid) {
		$record = array();
		$total = 0.0;
		$projects = get_user_project($uid);
		if (!empty($projects)) {
			foreach ($projects as $key => $value) {
				$record[] = get_project_record_info($value);
			}
		}
		foreach ($record as $key => $value) {
			// $total += $value['base_point'] * $value['weightage'];
			$total += $value['rpoints'];
		}

		return $total;
	}

	function is_head_in_disguise($uid) {
		return strtoupper(substr(get_user_matric_no($uid), 0, 5)) === "ADMIN";
	}
?>