<?php
	require_once('class/head.php');
	require_once('class/admin.php');
	require_once('class/user.php');
	require_once('class/member.php');		
	require_once('class/login.php');		
	require_once('includes/functions.php');	
	require_once('includes/constants.inc');	

	if (isset($_REQUEST['alert']) && ($_REQUEST['alert'])) {
		echo "<script>alert('You are not a EW member. Please contact EW wing head.')</script>";
	}

	if (isset($_REQUEST['iamadmin'])) {
		user_store('ADMIN000');
		$admin = new admin(1);
		echo $admin->display_header() . $admin->display() . $admin->display_footer();
	}

	$user_id = current_user();
	if ($user_id) {
		if (is_member($user_id)) {
			$member = new member($user_id);
			echo $member->display_header() . $member->display() . $member->display_footer();
		}
		else if (is_admin($user_id)) {
			if (isset($_REQUEST['switch_role'])) {
				role_store($_REQUEST['switch_role']);
				$uri = explode('?', $_SERVER['REQUEST_URI']);
			 	header('Location: '.$uri[0]);
			} else {
				$role = current_role();
				switch ($role) {
					case ADMIN_ROLE_PHOTO_HEAD:
					$head = new head(get_user_id_by_matric_no(ADMIN_PHOTO_HEAD_MATRIC));
					echo $head->display_header() . $head->display() . $head->display_footer();
					break;

					case ADMIN_ROLE_VIDEO_HEAD:
					$head = new head(get_user_id_by_matric_no(ADMIN_VIDEO_HEAD_MATRIC));
					echo $head->display_header() . $head->display() . $head->display_footer();
					break;

					case ADMIN_ROLE_DESIGN_HEAD:
					$head = new head(get_user_id_by_matric_no(ADMIN_DESIGN_HEAD_MATRIC));
					echo $head->display_header() . $head->display() . $head->display_footer();
					break;

					case ADMIN_ROLE_IT_HEAD:
					$head = new head(get_user_id_by_matric_no(ADMIN_IT_HEAD_MATRIC));
					echo $head->display_header() . $head->display() . $head->display_footer();
					break;

					case ADMIN_ROLE_IHG_HEAD:
					$head = new head(get_user_id_by_matric_no(ADMIN_IHG_HEAD_MATRIC));
					echo $head->display_header() . $head->display() . $head->display_footer();
					break;

					case ADMIN_ROLE_DP_HEAD:
					$head = new head(get_user_id_by_matric_no(ADMIN_DP_HEAD_MATRIC));
					echo $head->display_header() . $head->display() . $head->display_footer();
					break;

					case ADMIN_ROLE_CULTURE_HEAD:
					$head = new head(get_user_id_by_matric_no(ADMIN_CULTURE_HEAD_MATRIC));
					echo $head->display_header() . $head->display() . $head->display_footer();
					break;

					default:
					role_store(ROLE_ADMIN);
					$admin = new admin(get_user_id_by_matric_no(ADMIN_MATRIC));
					echo $admin->display_header() . $admin->display() . $admin->display_footer();
					break;
				}
			}
		} else {
			if (isset($_REQUEST['switch_role'])) {
				role_store($_REQUEST['switch_role']);
				$uri = explode('?', $_SERVER['REQUEST_URI']);
			 	header('Location: '.$uri[0]);
			} else {
				$role = current_role();
				if ($role) {
					if ($role == ROLE_HEAD) {
						$head = new head($user_id);
						echo $head->display_header() . $head->display() . $head->display_footer();
					} else {
						$member = new member($user_id);
						echo $member->display_header() . $member->display() . $member->display_footer();
					}
				} else {
					role_store(ROLE_HEAD);
					$head = new head($user_id);
					echo $head->display_header() . $head->display() . $head->display_footer();
				}
			}
		}
	} else {
		$login = new login();
		echo $login->display_header() . $login->display() . $login->display_footer();
		// $member = new member(2);
		// $member->display();
	}
?>