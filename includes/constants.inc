<?php
	define('HOST', 'localhost');

	define('DB_HOST', 'localhost');
	define('DB_USERNAME', 'username');
	define('DB_PWD', 'password');
	define('DB_NAME', 'point_system');

	define('ROLE_TABLE', 'role');
	define('WING_TABLE', 'wing');
	define('USER_TABLE', 'user');
	define('PROJECT_TABLE', 'project');
	define('RECORD_TABLE', 'record');
	define('MSW_TABLE', 'msw');

	define('ROLE_ADMIN', 1);
	define('ROLE_WING_HEAD', 2);
	define('ROLE_MSW_HEAD', 3);
	define('ROLE_MEMBER', 4);
	define('ROLE_HEAD', 2.5);

	define('PROJECT_STATUS_NOT_STARTED', 'not started');
	define('PROJECT_STATUS_IN_PROGRESS', 'in progress');
	define('PROJECT_STATUS_FINISHED', 'finished');
	define('PROJECT_STATUS_CANCELLED', 'cancelled');

	define('WING_NULL', 1);
	define('WING_PHOTO', 2);
	define('WING_VIDEO', 3);
	define('WING_DESIGN', 4);
	define('WING_IT', 5);
	define('WING_OTHERS', 6);

	define('MSW_NULL', 1);
	define('MSW_IHG', 2);
	define('MSW_DP', 3);
	define('MSW_CULTURE', 4);
	define('MSW_CROSS', 5);

	define('ADMIN_ROLE_PHOTO_HEAD', 2.1);
	define('ADMIN_ROLE_VIDEO_HEAD', 2.2);
	define('ADMIN_ROLE_DESIGN_HEAD', 2.3);
	define('ADMIN_ROLE_IT_HEAD', 2.4);
	define('ADMIN_ROLE_IHG_HEAD', 3.1);
	define('ADMIN_ROLE_DP_HEAD', 3.2);
	define('ADMIN_ROLE_CULTURE_HEAD', 3.3);

	define('ADMIN_MATRIC', 'ADMIN000');
	define('ADMIN_PHOTO_HEAD_MATRIC', 'ADMIN001');
	define('ADMIN_VIDEO_HEAD_MATRIC', 'ADMIN002');
	define('ADMIN_IT_HEAD_MATRIC', 'ADMIN003');
	define('ADMIN_DESIGN_HEAD_MATRIC', 'ADMIN004');
	define('ADMIN_CULTURE_HEAD_MATRIC', 'ADMIN005');
	define('ADMIN_DP_HEAD_MATRIC', 'ADMIN006');
	define('ADMIN_IHG_HEAD_MATRIC', 'ADMIN007');

?>