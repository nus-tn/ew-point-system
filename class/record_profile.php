<?php
/**
* this class seems not being used at all :-p
*/
require_once("includes/functions.php");
require_once("class/page.php");

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}


class record_profile extends page
{	
	var $rcid;
	var $uid;
	var $pid;
	var $weightage;
	var $comments;
	var $caller;

	public function __construct($rcid, $caller) {
		$this->rcid      = $rcid;
		$this->uid       = get_record_uid($rcid);
		$this->pid       = get_record_pid($rcid);
		$this->weightage = get_record_weightage($rcid);
		$this->comments  = get_record_comments($rcid);
		$this->caller    = $caller;
	}
	
	public function display() {
		if (is_valid_id($this->rcid, 'record')) {
			return $this->caller->display_menu() . $this->display_profile();
		} else {
			return $this->caller->display_menu() . $this->display_error();
		}
	}

	private function display_error() {
		return <<<INVALID_ID
			<p></p>
			<div style="text-align:center; color:#AAA">Invalid record id : {$this->rcid}
			</div>
INVALID_ID;
	}

	private function display_profile()
	{
		$disabled    = 'disabled="disabled"';
		$manageable  = (is_admin($this->caller->uid) || is_head_of_project($this->caller->uid, $this->pid)) ? '' : $disabled;
		$submit	 	 = (is_admin($this->caller->uid) || is_head_of_project($this->caller->uid, $this->pid)) ? '' : 'style="display : none"';

		$project_name       = get_project_name($this->pid);
		$project_base_point = get_project_base_point($this->pid);
		$weightage          = get_record_weightage($this->rcid);
		$user_name          = get_user_name($this->uid);

		$selected = array();
		for ($i = 0; $i <= 20; $i ++) { 
			$selected[$i] = '';
		}
		$selected[$weightage * 10] = 'selected';
		$weightage_options = '';
		for ($i = 0; $i <= 20; $i ++) { 
			$wei = number_format($i / 10, 1);
			$weightage_options .= "<option value='$wei'" . $selected[$i] . ">$wei</option>";
		}
		


		$output = <<<PROJECT_INFO
		<form name='profile-form' method='POST' id='profile-form'>
			<table class="table table-hover">
				<tr>
					<td>
						<label>Project Name</label>
					</td>
					<td>
						$project_name
					</td>
				</tr>
				<tr>
					<td>
						<label>Assignee</label>
					</td>
					<td>
						$user_name
					</td>
				</tr>
				<!--<tr>
					<td>
						<label>Project Base Point</label>
					</td>
					<td>
						project_base_point
					</td>
				</tr>
				<tr>
					<td>
						<label for='profile-weightage'>Weightage</label>
					</td>
					<td>
						<select name='profile-weightage' class="form-control">
							$weightage_options;
						</select>
					</td>
				</tr>-->
				<tr>
					<td>
						<label for='profile-comments'>Comments</label>
					</td>
					<td>
						<input type='text' class="form-control" name='profile-comments' $manageable value='{$this->comments}'/>
					</td>
				</tr>
				<tr>
					<td><input type='submit' class="btn btn-default" $submit name='update-record' value='Update'></td>
				</tr>
			</table>
		</div>
PROJECT_INFO;
		return $output;
	}
}
?>