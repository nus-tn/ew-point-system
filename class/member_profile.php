<?php
/**
* 		
*/
require_once("includes/functions.php");
require_once("class/page.php");

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}


class member_profile extends page
{	
	var $uid;
	var $user_name;
	var $matric_no;
	var $wid;
	var $mid;
	var $rid;
	var $email;
	var $room;
	var $contact;
	var $projects;
	var $caller;

	public function __construct($uid, $caller) {
		$this->uid       = $uid;
		$this->matric_no = get_user_matric_no($uid);
		$this->user_name = get_user_name($uid);
		$this->wid       = get_user_wing($uid);
		$this->mid       = get_user_msw($uid);
		$this->rid       = get_user_role($uid);
		$this->email 	 = get_user_email($uid);
		$this->room 	 = get_user_room($uid);
		$this->contact 	 = get_user_contact($uid);

		$projects        = get_user_project($this->uid);
		if (!empty($projects)) {
			foreach ($projects as $key => $value) {
				$this->projects[] = get_project_record_info($value);
			}
		}
		$this->caller = $caller;
	}
	
	public function display() {
		if (is_valid_id($this->uid, 'member')) {
			return $this->caller->display_menu() . $this->display_profile();
		} else {
			return $this->caller->display_menu() . $this->display_error();
		}
	}

	private function display_error() {
		return <<<INVALID_ID
			<p></p>
			<div style="text-align:center; color:#AAA">Invalid user id : {$this->uid}
			</div>
INVALID_ID;
	}

	private function display_profile()
	{
		$wing_name     = get_wing_name($this->wid);
		$msw_name      = get_msw_name($this->mid);
		$total_point   = get_user_total_point($this->uid) . ' / 25';
		$disabled      = 'disabled="disabled"';
		$manageable    = (is_admin($this->caller->uid) || ($this->caller->uid == $this->uid) || is_head_of_member($this->caller->uid, $this->uid)) ? '' : $disabled;
		$super_manageable    = (is_admin($this->caller->uid) || is_head_of_member($this->caller->uid, $this->uid)) ? '' : $disabled;
		$submit        = (is_admin($this->caller->uid) || ($this->caller->uid == $this->uid) || is_head_of_member($this->caller->uid, $this->uid)) ? '' : 'style="display : none"';
		$delete_submit = (is_admin($this->caller->uid) || is_head_of_member($this->caller->uid, $this->uid)) ? '' : 'style="display : none"';

		$select_wing   = array('', '', '', '', '', '', '');
		$select_wing[$this->wid] = 'selected';
		$wing_option   = "<option value='" . WING_NULL . "'" . $select_wing[WING_NULL] . ">" . get_wing_name(WING_NULL) . "</option>" . 
						 "<option value='" . WING_PHOTO . "'" . $select_wing[WING_PHOTO] . ">" . get_wing_name(WING_PHOTO) . "</option>" . 
						 "<option value='" . WING_VIDEO . "'" . $select_wing[WING_VIDEO] . ">" . get_wing_name(WING_VIDEO) . "</option>" . 
						 "<option value='" . WING_DESIGN . "'" . $select_wing[WING_DESIGN] . ">" . get_wing_name(WING_DESIGN) . "</option>" . 
						 "<option value='" . WING_IT . "'" . $select_wing[WING_IT] . ">" . get_wing_name(WING_IT) . "</option>" . 
						 "<option value='" . WING_OTHERS . "'" . $select_wing[WING_OTHERS] . ">" . get_wing_name(WING_OTHERS) . "</option>"  ;

		$select_msw    = array('', '', '', '', '');
		$select_msw[$this->mid] = 'selected';
		$msw_option   = "<option value='" . MSW_NULL . "'" . $select_msw[MSW_NULL] . ">" . get_msw_name(MSW_NULL) . "</option>" . 
						 "<option value='" . MSW_IHG . "'" . $select_msw[MSW_IHG] . ">" . get_msw_name(MSW_IHG) . "</option>" . 
						 "<option value='" . MSW_DP . "'" . $select_msw[MSW_DP] . ">" . get_msw_name(MSW_DP) . "</option>" . 
						 "<option value='" . MSW_CULTURE . "'" . $select_msw[MSW_CULTURE] . ">" . get_msw_name(MSW_CULTURE) . "</option>" ;
						 
		$output        = <<<MEMBER_INFO

		<div>
			<h4 class="text-center">View profile</h4>
		</div>
		<form name='profile-form' method='POST' id='profile-form'>
			<div class="form-group">
				<table class="table table-hover">
					<tr>
						<td>
							<label>Matric Number</label>
						</td>
						<td>
							<input type='hidden' name='profile-uid' value={$this->uid} />
							{$this->matric_no}
						</td>
					</tr>
					<tr>
						<td>
							<label>Name</label>
						</td>
						<td>
							{$this->user_name}
						</td>
					</tr>
					<tr>
						<td>
							<label>Specialization Wing</label>
						</td> 
						<td>
							<select name='profile-wing' $super_manageable class="form-control">
								$wing_option
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<label>Media Support Wing</label>
						</td>
						<td>
							<select name='profile-msw' $super_manageable class="form-control">
								$msw_option
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<label>Total Points</label>
						</td>
						<td>
							$total_point
						</td>
					</tr>
					<tr>
						<td>
							<label for='profile-email'>Email</label>
						</td>
						<td>
							<input type='text' class="form-control" name='profile-email' $manageable value='{$this->email}'/>
						</td>
					</tr>
					<tr>
						<td>
							<label for='profile-room'>Room Number</label>
						</td>
						<td>
							<input type='text' class="form-control" name='profile-room' $manageable value='{$this->room}'/>
						</td>
					</tr>
					<tr>
						<td>
							<label for='profile-contact'>Contact</label>
						</td>
						<td>
							<input type='text' class="form-control" name='profile-contact' $manageable value='{$this->contact}'/>
						</td>
					</tr>
					<tr>
						<td style="text-align: center" colspan=2>
							<input type='submit' class="btn btn-default" $submit name='update-member' value='Update'>
							<input type='submit' class="btn btn-danger" $delete_submit name='delete-member' value='Delete'>
						</td>
					</tr>
				</table>
			</div>
MEMBER_INFO;
		return $output;
	}
}
?>