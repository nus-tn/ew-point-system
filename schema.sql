DROP TABLE IF EXISTS role;

CREATE TABLE role (
	rid INT AUTO_INCREMENT,
	rname VARCHAR(10) NOT NULL,
	PRIMARY KEY(rid)
);

DROP TABLE IF EXISTS wing;

CREATE TABLE wing (
	wid INT AUTO_INCREMENT,
	wname VARCHAR(10) NOT NULL,
	PRIMARY KEY(wid)
);

DROP TABLE IF EXISTS msw;

CREATE TABLE msw (
	mid INT AUTO_INCREMENT,
	mname VARCHAR(10) NOT NULL,
	PRIMARY KEY(mid)
);

DROP TABLE IF EXISTS user;

CREATE TABLE user (
	uid INT AUTO_INCREMENT,
	matric VARCHAR(8) NOT NULL, # force dropping the last letter
	name VARCHAR(100) NOT NULL,
	rid INT NOT NULL,
	wid INT NOT NULL,
	mid INT NOT NULL,
	email VARCHAR(100) NOT NULL,
	room VARCHAR(4) NOT NULL,
	contact VARCHAR(8) NOT NULL,
	deleted INT NOT NULL DEFAULT 0,
	FOREIGN KEY (rid) REFERENCES role(rid),
	FOREIGN KEY (wid) REFERENCES wing(wid),
	FOREIGN KEY (mid) REFERENCES msw(mid),
	PRIMARY KEY (uid)
);

DROP TABLE IF EXISTS project;

CREATE TABLE project (
	pid INT AUTO_INCREMENT,
	wid INT NOT NULL,
	mid INT NOT NULL,
	pname VARCHAR(100) NOT NULL,
	pdescription VARCHAR(1000),
	ppoint INT NOT NULL,
	deleted INT NOT NULL DEFAULT 0,
	pstatus ENUM('not started','in progress','finished','cancelled') DEFAULT 'not started',
	FOREIGN KEY (wid) REFERENCES wing(wid),
	FOREIGN KEY (mid) REFERENCES msw(mid),
	PRIMARY KEY (pid)
);

DROP TABLE IF EXISTS record;

CREATE TABLE record (
	rcid INT AUTO_INCREMENT,
	pid INT NOT NULL,
	uid INT NOT NULL,
	rpoints DOUBLE DEFAULT 0.0,
	comments VARCHAR(1000),
	deleted INT NOT NULL DEFAULT 0,
	FOREIGN KEY (pid) REFERENCES project(pid),
	FOREIGN KEY (uid) REFERENCES user(uid),
	PRIMARY KEY (rcid)
);


INSERT INTO `role` (`rname`) VALUES ('Admin');
INSERT INTO `role` (`rname`) VALUES ('Wing Head');
INSERT INTO `role` (`rname`) VALUES ('MSW Head');
INSERT INTO `role` (`rname`) VALUES ('Member');

INSERT INTO `wing` (`wname`) VALUES ('N/A');
INSERT INTO `wing` (`wname`) VALUES ('Photo');
INSERT INTO `wing` (`wname`) VALUES ('Video');
INSERT INTO `wing` (`wname`) VALUES ('Design');
INSERT INTO `wing` (`wname`) VALUES ('IT');
INSERT INTO `wing` (`wname`) VALUES ('Others');

INSERT INTO `msw` (`mname`) VALUES ('N/A');
INSERT INTO `msw` (`mname`) VALUES ('IHG');
INSERT INTO `msw` (`mname`) VALUES ('DP');
INSERT INTO `msw` (`mname`) VALUES ('Culture');
INSERT INTO `msw` (`mname`) VALUES ('Cross-MSW');

INSERT INTO `user` (`matric`, `name`, `rid`, `wid`, `mid`) VALUES ('ADMIN001', 'Photo admin', 2, 2, 1);
INSERT INTO `user` (`matric`, `name`, `rid`, `wid`, `mid`) VALUES ('ADMIN002', 'Video admin', 2, 3, 1);
INSERT INTO `user` (`matric`, `name`, `rid`, `wid`, `mid`) VALUES ('ADMIN003', 'IT admin', 2, 5, 1);
INSERT INTO `user` (`matric`, `name`, `rid`, `wid`, `mid`) VALUES ('ADMIN004', 'Design admin', 2, 4, 1);
INSERT INTO `user` (`matric`, `name`, `rid`, `wid`, `mid`) VALUES ('ADMIN005', 'Culture admin', 3, 1, 4);
INSERT INTO `user` (`matric`, `name`, `rid`, `wid`, `mid`) VALUES ('ADMIN006', 'DP admin', 3, 1, 3);
INSERT INTO `user` (`matric`, `name`, `rid`, `wid`, `mid`) VALUES ('ADMIN007', 'IHG admin', 3, 1, 2);

