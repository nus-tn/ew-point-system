<?php
/**
* 		
*/
require_once('includes/db.php');
require_once('class/user.php');
require_once('class/admin.php');
require_once('class/member_profile.php');
require_once('class/project_profile.php');
require_once('class/record_profile.php');

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

class head extends user
{	

	function __construct($uid) {
		parent :: __construct($uid);
	}

	public function display() {
		if (is_admin($this->uid)) {
			$output = <<<ADMIN_HOME_HEADER
				<br />
				<div>
					<h4 class="text-center">Welcome admin! We haven't implemented your part yet :-)</h4>
				</div>
ADMIN_HOME_HEADER;

			return $output;
		}
		// print_r($_SESSION);
		// if (is_admin($this->uid)) {
		if (TRUE) {
			// admin
			if (isset($_GET['operation'])) {

				switch ($_GET['operation']) {
					case 'add-project':
						if (isset($_REQUEST['add-project'])) {
							$wid = WING_NULL;
							$mid = MSW_NULL;
							if (is_wing_head($this->uid)) {
								$wid = $this->wid;
							} else {
								$mid = $_REQUEST['msw'];
							}
							if (add_project($_REQUEST['project-name'], 
											$_REQUEST['project-point'], 
											$wid, 
											$mid, 
											$_REQUEST['project-description'])) {
								//alert("project created");
								$_SESSION['mode'] = 'project';
								return $this->display_menu() . $this->display_add_project();
							} else {
								$_SESSION['mode'] = 'project';
								alert("creating project failed");
							}
						} else {
							$_SESSION['mode'] = 'project';
							return $this->display_menu() . $this->display_add_project();
						}
						break;

					case 'view-project':
						if (isset($_REQUEST['pid'])) {
							if (isset($_REQUEST['update-project'])) {
								$assignee = array();
								if (is_wing_head($this->uid)) {
									foreach (get_wing_member($this->wid) as $key => $value) {
										if (isset($_REQUEST["member-$value"]) && ($_REQUEST["member-$value"])) {
											$assignee[] = $value;
										}
									}
								} else if (is_msw_head($this->uid)) {
									foreach (get_msw_member(MSW_CROSS) as $key => $value) {//hack here
										if (isset($_REQUEST["member-$value"]) && ($_REQUEST["member-$value"])) {
											$assignee[] = $value;
										}
									}
								}
								$assigned = get_project_rcid($_REQUEST['pid']);
								$flag = TRUE;
								foreach ($assigned as $assigned_rcid) {
									if (!update_project_record($assigned_rcid,
															   $_REQUEST['record-points-' . $assigned_rcid],
															   $_REQUEST['record-comments-' . $assigned_rcid])) {
										$flag = FALSE;
										break;
									}
								}
								if ($flag) {
									if (update_project($_REQUEST['pid'],
													   $_REQUEST['profile-name'],
													   $_REQUEST['profile-point'],
													   $_REQUEST['profile-description'],
													   $assignee)) {
										//alert("project updated");
										$profile = new project_profile($_REQUEST['pid'], $this);
										$_SESSION['mode'] = 'project';
										return $profile->display();
									} else {
										alert("updating project failed");
										$_SESSION['mode'] = 'project';
										return $profile->display();
									}
								} else {
									alert("updating project failed");
									$_SESSION['mode'] = 'project';
									return $profile->display();
								}
							} else if (isset($_REQUEST['delete-project'])){
								if (delete_project($_REQUEST['pid'])) {
									//alert("project deleted");
									$_SESSION['mode'] = 'project';
									return $this->display_menu() . $this->display_view_projects();
								} else {
									alert("deleting project failed");
									$_SESSION['mode'] = 'project';
								}
							} else if (isset($_REQUEST['start-project'])) {
								if (start_project($_REQUEST['pid'])) {
									//alert("project started");
									$profile = new project_profile($_REQUEST['pid'], $this);
									$_SESSION['mode'] = 'project';
									return $profile->display();
								} else {
									alert("starting project failed");
									$_SESSION['mode'] = 'project';
								}
							} else if (isset($_REQUEST['end-project'])){
								if (end_project($_REQUEST['pid'])) {
									//alert("project ended");
									$profile = new project_profile($_REQUEST['pid'], $this);
									$_SESSION['mode'] = 'project';
									return $profile->display();
								} else {
									alert("ending project failed");
									$_SESSION['mode'] = 'project';
								}
							} else {
								$profile = new project_profile($_REQUEST['pid'], $this);
								$_SESSION['mode'] = 'project';
								return $profile->display();
							}
						} else {
							$_SESSION['mode'] = 'project';
							return $this->display_menu() . $this->display_view_projects();
						}
						break;

					case 'add-member':
						if (isset($_REQUEST['add-member'])) {
							if (add_member($_REQUEST['member-matric-no'],
										   $_REQUEST['member-name'],
										   ROLE_MEMBER,
										   $this->wid,
										   $this->mid,
										   $_REQUEST['member-email'],
										   $_REQUEST['member-room'],
										   $_REQUEST['member-contact'])) {
								//alert("member created");
								$_SESSION['mode'] = 'member';
								return $this->display_menu() . $this->display_add_member();
							} else {
								$_SESSION['mode'] = 'member';
								alert("creating member failed");
							}
						} else if (isset($_REQUEST['add-msw-member'])) {
							$member = array_merge(get_wing_member(WING_PHOTO), get_wing_member(WING_VIDEO));
							$msw_member = array();
							foreach ($member as $key => $value) {
								if (isset($_REQUEST["member-$value"]) && ($_REQUEST["member-$value"])) {
									$msw_member[] = $value;
								}
							}

							if (update_msw_member($this->mid, $msw_member)) {
								// success
								$_SESSION['mode'] = 'home';
								return $this->display_menu() . $this->display_admin();
							} else {
								//failed
								$_SESSION['mode'] = 'home';
								return $this->display_menu() . $this->display_admin();
							}
						} else {
							$_SESSION['mode'] = 'member';
							return $this->display_menu() . $this->display_add_member();
						}
						break;

					case 'view-member':
						if (isset($_REQUEST['uid'])) {
							if (isset($_REQUEST['update-member'])) {
								if (update_member($_REQUEST['profile-uid'],
												  $_REQUEST['profile-wing'],
												  $_REQUEST['profile-msw'], 
												  $_REQUEST['profile-email'], 
												  $_REQUEST['profile-room'], 
												  $_REQUEST['profile-contact'])) {
									//alert("member updated");
									$profile = new member_profile($_REQUEST['uid'], $this);
									$_SESSION['mode'] = 'member';
									return $profile->display();
								} else {
									alert("updating member failed");
									$_SESSION['mode'] = 'member';
								}
							} else if (isset($_REQUEST['delete-member'])) {
								if (delete_member($_REQUEST['profile-uid'])) {
									//alert("member deleted");
									$_SESSION['mode'] = 'member';
									return $this->display_menu() . $this->display_view_member();
								} else {
									alert("deleting member failed");
									$_SESSION['mode'] = 'member';
								}
							} else {
								$profile = new member_profile($_REQUEST['uid'], $this);
								$_SESSION['mode'] = 'member';
								return $profile->display();
							}
						} else {
							$_SESSION['mode'] = 'member';
							return $this->display_menu() . $this->display_view_member();
						}
						break;

					case 'view-record':
						if (isset($_REQUEST['rcid'])) {
							if (isset($_REQUEST['update-record'])) {
								if (update_project_record($_REQUEST['rcid'], 
														  $_REQUEST['profile-points'], 
														  $_REQUEST['profile-comments'])) {
									//alert("record updated");
									$profile = new record_profile($_REQUEST['rcid'], $this);
									$_SESSION['mode'] = 'project';
									return $profile->display();
								} else {
									alert("updating record failed");
									$_SESSION['mode'] = 'project';
								}
							} else {
								$profile = new record_profile($_REQUEST['rcid'], $this);
								$_SESSION['mode'] = 'project';
								return $profile->display();
							}
						} else {
							// TODO : record management page
							$_SESSION['mode'] = 'project';
							return $this->display_menu() . $this->display_view_projects();
						}
						break;
					
					case 'log-out':
						return $this->log_out();
						break;

					default:
						# code...
						break;
				}
			} else if (isset($_REQUEST['record-pid'])){
				$rcid = get_project_rcid($_REQUEST['record-pid']);
				foreach ($rcid as $value) {
					if (isset($_REQUEST['manage-record-' . $value])) {
						switch ($_REQUEST['manage-record-' . $value]) {
							case 'Update':
								if (update_project_record($value,
													  $_REQUEST['record-points-' . $value],
													  $_REQUEST['record-comments-' . $value])) {
									// alert("record updated");
									$_SESSION['mode'] = 'home';
									return $this->display_menu() . $this->display_admin();
								} else {
									// alert("updating record failed");
									$_SESSION['mode'] = 'home';
									return $this->display_menu() . $this->display_admin();
								}
								break;

							case 'Remove':
								if (delete_project_record($value)) {
									// alert("record removed");
									$_SESSION['mode'] = 'home';
									return $this->display_menu() . $this->display_admin();
								} else {
									// alert("removing record failed");
									$_SESSION['mode'] = 'home';
									return $this->display_menu() . $this->display_admin();
								}
								break;
							
							default:
								# code...
								break;
						}
					}
				}
			} else {
				$_SESSION['mode'] = 'home';
				return $this->display_menu() . $this->display_admin(); 
			}
		}
		else {
			$_SESSION['mode'] = 'home';
			return $this->display_login(); 
		}
	}

	private function display_login() {
		$output = <<<ADMIN_LOGIN

			<form method="POST">
				<label for="password"></label>
				<input type="password" size="25" placeholder="Password" name="password"/>
				<input type="submit" class="btn btn-default" value="Log In" name="login"/>
			</form>

ADMIN_LOGIN;

		if (isset($_POST['login'])) {
			if ($this->admin_login()) {
				return $this->display_admin();
			} else {
				$output = "<div>Please enter a valid password.</div>" . $output;
			}
		} 

		return $output;
	}

	private function display_admin() {		
		//display the default admin page 
		$title = '';
		$projects = array();
		$members = array();
		$members_name = array();

		if (is_admin($this->uid)) {
			$output = <<<ADMIN_HOME_HEADER
				<br />
				<div>
					<h4 class="text-center">Welcome admin! We haven't implemented your part yet :-)</h4>
				</div>
ADMIN_HOME_HEADER;

			return $output;
		} else {

			if (is_wing_head($this->uid)) {
				$projects = get_wing_unfinished_project($this->wid);
				$members = get_wing_member($this->wid);
				$title = get_wing_name($this->wid) . ' wing overview';
			} else if (is_msw_head($this->uid)){
				$projects = get_msw_unfinished_project($this->mid);
				$members = get_msw_member($this->mid);
				$title = get_msw_name($this->mid) . ' msw overview';
			} else {
				// TODO: invalid access
			}

			$output = <<<ADMIN_HOME_HEADER
				<div>
					<h4 class="text-center">$title</h4>
				</div>
ADMIN_HOME_HEADER;


			// Member List
			foreach ($members as $key => $value) {
				$members_name[$key] = get_user_name($value);
				if (is_head($value)) {
					$members_name[$key] .= ' <span style="color:red">&#10040;</span>';
				}
			}
			for ($i = 1; $i <= count($members) % 4; $i++) { 
				$members_name[] = '';
				$members[] = -1;
			}
			$output .= '<table class="table table-plain" style="text-align : center">';
			for ($i = 0; $i < count($members) / 4 ; $i++) { 
				$output .=	<<<ADMIN_HOME_MEMBER
					<tr>
						<td style="width : 25%"><a href='?operation=view-member&uid={$members[$i * 4]}'>{$members_name[$i * 4]}</a></td>
						<td style="width : 25%"><a href='?operation=view-member&uid={$members[$i * 4 + 1]}'>{$members_name[$i * 4 + 1]}</a></td>
						<td style="width : 25%"><a href='?operation=view-member&uid={$members[$i * 4 + 2]}'>{$members_name[$i * 4 + 2]}</a></td>
						<td style="width : 25%"><a href='?operation=view-member&uid={$members[$i * 4 + 3]}'>{$members_name[$i * 4 + 3]}</a></td>
					</tr>
ADMIN_HOME_MEMBER;
			}
			$output .= '</table>';
			

			// Projects List
			foreach ($projects as $key => $value) {
				$project_name = get_project_name($value);
				$project_description = get_project_description($value);
				$project_base_point = get_project_base_point($value);
				$output .= <<<ADMIN_HOME_PROJECTS_INFO
				<form method="POST">
					<input type="hidden" name="record-pid" value=$value />
					<table class="table table-hover">
						<div>
							<h4><a id='$value' class='project-title' style="color:#000; text-decoration:none" href='#'>$project_name<span class="caret"></span></a></h4>
							<div id='project-details-$value' style='display:none;'>
								<ul>
									<li>Description : $project_description</li>
									<li>Base Point  : $project_base_point</li>
									<li><a href='?operation=view-project&pid=$value'>View project detail</a>
								</ul>							
							</div>	
						</div>
ADMIN_HOME_PROJECTS_INFO;

				$output .= <<<ADMIN_HOME_PROJECT
						<tr>
							<th>Name</th>
							<th>Points</th>
							<th>Comments</th>
							<th>Options</th>
						</tr>
ADMIN_HOME_PROJECT;
				$assignee = get_project_assignee($value);
				if (count($assignee) == 0) {
					$output .= <<<NO_ASSIGNEE_MESSAGE
						<tr>
							<td colspan=5>
								<div style="text-align:center; color:#AAA">This project has not been assigned to anyone yet.
								</div>
							</td>
						</tr>
NO_ASSIGNEE_MESSAGE;
				} else {
					foreach ($assignee as $id) {
						$rcid = get_record_id($value, $id);

						$assignee_name      = get_user_name($id);
						// $assignee_point	    = get_record_weightage($rcid);
						$record_points      = get_record_points($rcid);
						$record_comments    = get_record_comments($rcid);

						// $selected = array();
						// for ($i = 0; $i <= 20; $i ++) { 
						// 	$selected[$i] = '';
						// }
						$weightage_options = '';
						// $selected[$assignee_weightage * 10] = 'selected';
						// $weightage_options = '';
						// for ($i = 0; $i <= 20; $i ++) { 
						// 	$wei = number_format($i / 10, 1);
						// 	$weightage_options .= "<option value='$wei'" . $selected[$i] . ">$wei</option>";
						// }

						$output .= <<<ADMIN_HOME_ASSIGNEE
							<tr>
								<td width='25%'>$assignee_name</td>
								<!--<td>
									<select name='record-weightage-$rcid' class="form-control">
										$weightage_options
									</select>
								</td>
								<td>
									$record_points
								</td>-->
								<td width='10%'>
									<input type='text' value=$record_points name='record-points-$rcid' class="form-control">
								</td>
								<td>
									<textarea class='form-control' name='record-comments-$rcid' placeholder='Enter comment here...' >$record_comments</textarea>
									</td>
								<td class="td-options text-center">
									<input type='submit' class="btn btn-default update" name='manage-record-$rcid' value='Update'>
									<input type='submit' class="btn btn-danger remove" name='manage-record-$rcid' value='Remove'>
								</td>
							</tr>
ADMIN_HOME_ASSIGNEE;
					}
				}
				$output .= '</table></form>';
			}
		}

		return $output;
	}

	public function admin_login() {
		if (isset($_POST['password'])) {
			if ($_POST['password'] == 'it') {
				$_SESSION['admin-it'] = TRUE;
			}
			elseif ($_POST['password'] == 'design') {
				$_SESSION['admin-design'] = TRUE;
			}
			elseif ($_POST['password'] == 'photo') {
				$_SESSION['admin-photo'] = TRUE;
			}
			elseif ($_POST['password'] == 'video') {
				$_SESSION['admin-video'] = TRUE;
			}
			elseif ($_POST['password'] == 'boss') {
				$_SESSION['admin-all'] = TRUE;
			} else {
				return FALSE;
			}
			return TRUE;
		}
		return FALSE;
	}

	private function is_admin() {
		if (isset($_SESSION['admin-all']) || isset($_SESSION['admin-video']) || isset($_SESSION['admin-photo']) 
			|| isset($_SESSION['admin-it']) || isset($_SESSION['admin-design'])) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function create_project() {

	}

	private function update_project() {

	}

	public function display_menu()	
	{	
		$rid = ROLE_MEMBER;
		$home_active = '';
		$project_active = '';
		$member_active = '';
		$help_active = '';
		$admin_tag = '';

		if (is_admin($this->uid)) {
			$admin_tag = 'style="display : none"';
		}
		if (!isset($_SESSION['mode'])) {
			$_SESSION['mode'] = 'home';
		}
		switch ($_SESSION['mode']) {
			case 'home':
				$home_active = 'active';
				break;
			
			case 'project':
				$project_active = 'active';
				break;

			case 'member':
				$member_active = 'active';
				break;

			case 'help':
				$help_active = 'active';
				break;

			default:
				# code...
				break;
		}

		$switch_back = is_head_in_disguise($this->uid) ? 'Unspoof' : 'Switch to member';
		return <<<ADMIN_MENU
		
		<ul class="nav nav-tabs" role="tablist">
			<li class="$home_active"><a href="?">Home</a></li>

			<li class="dropdown $project_active">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">
					Projects <span class="caret"></span>
				</a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="?operation=view-project">View Projects</a></li>
					<li><a href="?operation=add-project">Add Project</a></li>
				</ul>
			</li>

			<li class="dropdown $member_active">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">
					Members <span class="caret"></span>
				</a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="?operation=view-member">View Members</a></li>
					<li><a href="?operation=add-member">Add Member</a></li>
				</ul>
			</li>

			<!--<li><a href="#" class="$help_active">Help</a></li>-->

			<li $admin_tag><a href="?switch_role=$rid">{$switch_back}</a></li>
			<li><a href="?operation=log-out">Log out</a></li>
		</ul>

ADMIN_MENU;
	}

	private function display_project_row_data($data){
		return <<<PROJECT_ROW_DATA
	
		<tr>
			<td>Tang Ning</td>
			<td align='center'>
				<select name='weightage' class="form-control">
					<option value="0.6">0.6</option>
					<option value="0.8">0.8</option>
					<option value="1.0" selected>1.0</option>
					<option value="1.2">1.2</option>
					<option value="1.4">1.4</option>
				</select>
			</td>
			<td>6</td>
			<td><textarea rows='2' cols='25' placeholder='Enter comment here...'></textarea></td>
		</tr>

PROJECT_ROW_DATA;
	}

	private function display_add_project(){
		$wing = FALSE;
		$ms = FALSE;
		if (is_wing_head($this->uid)) {
			$wing = get_wing_name($this->wid);
			$msw  = get_msw_name(1);
		} else {
			$wing = get_wing_name(1);
			$msw  = get_msw_name($this->mid);
			$msw  = '<select name="msw" class="form-control">
						<option value=' . $this->mid . '>' . $msw . '</option>
						<option value=' . MSW_CROSS . '>Cross-MSW</option>
					</select>';
		}
		
		return <<<ADMIN_ADD_PROJECT

		<div>
			<h4 class="text-center">Add project</h4>
		</div>
		<form name='add-project-form' method='POST' id='add-project-form'>
			<table class="table table-hover">
				<tr>
					<td>
						<label for='project-name'>Project Name</label>
					</td>
					<td><input type='text' class="form-control" name='project-name' /></td>
				</tr>
				<tr>
					<td>
						<label for='project-point'>Project Base Point</label>
					</td>
					<td><input type='text' class="form-control" name='project-point' /></td>
				</tr>
				<tr>
					<td><label for='project-wing'>Wing</label></td>
					<td>
						$wing
					</td>
				</tr>
				<tr>
					<td><label for='project-msw'>MSW</label></td>
					<td>
						$msw
					</td>
				</tr>
				<tr>
					<td><label for='project-description'>Project Description</label></td>
					<td><textarea type='textarea' class="form-control" name='project-description' form='add-project-form'></textarea></td>
				</tr>
				<tr>
					<td colspan=2 class="text-center">
						<input type='submit' class="btn btn-default" name='add-project' value='Add'>
					</td>
				</tr>
			</table>
		</form>

ADMIN_ADD_PROJECT;
	}

	private function display_view_projects() {
		$projects = array();
		$title = '';
		if (is_wing_head($this->uid)) {
			$projects = get_wing_project($this->wid);
			$title = get_wing_name($this->wid) . ' wing projects';
		} else {
			$projects = get_msw_project($this->mid);
			$title = get_msw_name($this->mid) . ' msw projects';
		}

		$rows = '';
		if (count($projects) == 0) {
			$rows = <<<NO_PROJECT
				<tr>
					<td colspan=5>
						<div style="text-align:center; color:#AAA">There is no project for this wing</div>
					</td>
				</tr>
NO_PROJECT;
		} else {
			foreach ($projects as $key => $value) {
				$project = get_project_info($value);
				// print_r($project);
				$assignee = '';
				$assignee_link = array();
				if (isset($project['assignee']) && (!empty($project['assignee']))) {
					foreach ($project['assignee'] as $uid) {
						$assignee_link[$uid] = "?operation=view-member&uid=" . $uid;
					}
				}

				$count = count($assignee_link);
				if ($count == 1) {
					foreach ($assignee_link as $id => $link) {
						$assignee = '<a href=' . $link . '>' . get_user_name($uid) . '</a>';
					}
				} else if ($count == 0) {
					$assignee = '';
				} else {
					$assignee = <<<DROPDOWN
						<div class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" style='text-decoration:none' href='#'>
							See $count assignees <span class="caret"></span>
							</a>
						<ul class="dropdown-menu" role="menu">
DROPDOWN;
					foreach ($assignee_link as $id => $link) {
						$user_name = get_user_name($id);
						$assignee .= <<<ASSIGNEE
								<li>
									<a href=$link>$user_name</a>
								</li>
ASSIGNEE;
					}
					$assignee .= <<<END_DROPDOWN
						</ul>
						</div>
END_DROPDOWN;
				}

				$project_name = '<a href="?operation=view-project&pid=' . $value . '">' . $project['name'] . '</a>';
				$rows .= <<<PROJECT
				
				<tr>
					<td>$project_name</td>
					<td>{$project['base_point']}</td>
					<td>{$project['description']}</td>
					<td>{$project['status']}</td>
					<td>$assignee</td>
				</tr>
PROJECT;
			}
		}

		$table = <<<ADMIN_VIEW_PROJECT
			
		<div>
			<h4 class="text-center">$title</h4>
		</div>
		<table class="table table-hover">
			<tr>
				<th>Project</th>
				<th>Base point</th>
				<th>Description</th>
				<th>Status</th>
				<th>Assignee</th>
			</tr>
	
ADMIN_VIEW_PROJECT;

		return $table . $rows . '</table>';
	}

	private function display_add_member(){
		if (is_wing_head($this->uid)) {
			$wing = get_wing_name($this->wid);
			$msw  = get_msw_name(MSW_NULL);
			return <<<ADMIN_ADD_MEMBER

			<div>
				<h4 class="text-center">Add member</h4>
			</div>
			<form name='add-member-form' method='POST' id='add-member-form'>
				<table class="table table-hover">
					<tr>
						<td>
							<label for='member-matric-no'>Member Matric Number</label>
						</td>
						<td>
							<input type='text' class="form-control" name='member-matric-no' />
						</td>
					</tr>
					<tr>
						<td>
							<label for='member-name'>Member Name</label>
						</td>
						<td>
							<input type='text' class="form-control" name='member-name' />
						</td>
					</tr>
					<tr>
						<td><label for='project-wing'>Wing</label></td>
						<td>
							$wing
						</td>
					</tr>
					<tr>
						<td><label for='project-msw'>MSW</label></td>
						<td>
							$msw
						</td>
					</tr>
					<tr>
						<td>
							<label for='member-email'>Email</label>
						</td>
						<td>
							<input type='text' class="form-control" name='member-email'/>
						</td>
					</tr>
					<tr>
						<td>
							<label for='member-room'>Room Number</label>
						</td>
						<td>
							<input type='text' class="form-control" name='member-room'/>
						</td>
					</tr>
					<tr>
						<td>
							<label for='member-contact'>Contact</label>
						</td>
						<td>
							<input type='text' class="form-control" name='member-contact' />
						</td>
					</tr>
					<tr>
						<td colspan=2 class="text-center">
							<input type='submit' class="btn btn-default" name='add-member' value='Add'>
						</td>
					</tr>
				</table>
			</form>

ADMIN_ADD_MEMBER;
		} else if (is_msw_head($this->uid)) {
			$output = '';

			$photo_member = get_wing_member(WING_PHOTO);
			$video_member = get_wing_member(WING_VIDEO);
			$msw_member = get_msw_member($this->mid);
			$list = array();

			foreach ($photo_member as $key => $value) {
				

                if (get_user_msw($value) != MSW_NULL && get_user_msw($value) != $this->mid) {
                    continue;
				}

				if (in_array($value, $msw_member)) {
					$list[] = "<div class='checkbox' ><input type='checkbox' checked='checked' disabled name='member-$value' value='TRUE' />" . get_user_name($value) . "</div>";
				} else {
					$list[] = "<div class='checkbox'><input type='checkbox' name='member-$value' value='TRUE' />" . get_user_name($value) . "</div>";
				}
			}
			$output .= <<<ADMIN_MSW_ADD_MEMBER_HEADER
				<table class="table table-plain" style="text-align : center">
					<tr><td colspan = '4'><h4 class="text-center">Photo wing members</h4></td></tr>
				<form class="form-control" name='add-msw-member-form' method='POST' id='add-msw-member-form'>
ADMIN_MSW_ADD_MEMBER_HEADER;

			for ($i = 1; $i <= count($list) % 4; $i++) { 
				$list[] = '';
			}

			for ($i = 0; $i < count($list) / 4 ; $i++) { 
				$output .=	<<<ADMIN_MSW_ADD_MEMBER
					<tr>
						<td style="width : 25%">{$list[$i * 4]}</td>
						<td style="width : 25%">{$list[$i * 4 + 1]}</td>
						<td style="width : 25%">{$list[$i * 4 + 2]}</td>
						<td style="width : 25%">{$list[$i * 4 + 3]}</td>
					</tr>
ADMIN_MSW_ADD_MEMBER;
			}

			$list = array();
			foreach ($video_member as $key => $value) {
				if (get_user_msw($value) != MSW_NULL && get_user_msw($value) != $this->mid) {
					continue;
				}
				
				if (in_array($value, $msw_member)) {
					$list[] = "<div class='checkbox' ><input type='checkbox' checked='checked' disabled name='member-$value' value='TRUE' />" . get_user_name($value) . "</div>";
				} else {
					$list[] = "<div class='checkbox'><input type='checkbox' name='member-$value' value='TRUE' />" . get_user_name($value) . "</div>";
				}
			}
			$output .= <<<ADMIN_MSW_ADD_MEMBER_HEADER
					<tr><td colspan = '4'><h4 class="text-center">Video wing members</h4></td></tr>
ADMIN_MSW_ADD_MEMBER_HEADER;

			for ($i = 1; $i <= count($list) % 4; $i++) { 
				$list[] = '';
			}

			for ($i = 0; $i < count($list) / 4 ; $i++) { 
				$output .=	<<<ADMIN_MSW_ADD_MEMBER
					<tr>
						<td style="width : 25%">{$list[$i * 4]}</td>
						<td style="width : 25%">{$list[$i * 4 + 1]}</td>
						<td style="width : 25%">{$list[$i * 4 + 2]}</td>
						<td style="width : 25%">{$list[$i * 4 + 3]}</td>
					</tr>
ADMIN_MSW_ADD_MEMBER;
			}
			$output .= <<<ADMIN_MSW_ADD_MEMBER_FOOTER
				<tr>
				<td colspan='4'>
					<input type='submit' class="btn btn-default update" name='add-msw-member' value='Add' />
				</td>
				</tr>
				</form>
				</table>
ADMIN_MSW_ADD_MEMBER_FOOTER;
			return $output;

		} else if (is_admin($this->uid)) {
			// TODO: admin view
		}
	}

	private function display_view_member() {
		$members = array();
		$title = '';
		if (is_wing_head($this->uid)) {
			$members = get_wing_member($this->wid);
			$title = get_wing_name($this->wid) . ' wing members';
		} else {
			$members = get_msw_member($this->mid);
			$title = get_msw_name($this->mid) . ' msw members';
		}

		$rows = '';
		foreach ($members as $key => $value) {
			$name = get_user_name($value);
			if ($value == $this->uid) {
				$name .= ' <span style="color:red">&#10040;</span>';
			}
			$matric = get_user_matric_no($value);
			$totalpoint = get_user_total_point($value) . ' / 25';
			$rows .= <<<PROJECT
			
			<tr>
				<td>
					<a href='?operation=view-member&uid=$value'>$name</a>
				</td>
				<td>$matric</td>
				<td>$totalpoint</td>
			</tr>
PROJECT;
		}

		$table = <<<ADMIN_VIEW_MEMBER
			
		<div>
			<h4 class="text-center">$title</h4>
		</div>
		<table class="table table-hover">
			<tr>
				<th>Name</th>
				<th>Matric Number</th>
				<th>Total Point</th>
			</tr>
	
ADMIN_VIEW_MEMBER;

		return $table . $rows . '</table>';
	}
}

?>
