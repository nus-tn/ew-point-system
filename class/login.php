<?php
/**
* 		
*/
require_once("includes/functions.php");
require_once("includes/constants.inc");
require_once("class/page.php");
require_once("class/openid.php");

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}


class login extends page
{	
	public function __construct() {

	}
	
	public function display() {
		return $this->display_login();
	}

	private function display_login() {

		try {
			$openid = new LightOpenID(HOST);
			if(!$openid->mode) {
				if (isset($_POST['nusnet_id'])) {
					$openid->identity = "https://openid.nus.edu.sg/".$_POST['nusnet_id'];
				}
				elseif (isset($_POST['openid_identifier'])) {
					$openid->identity = $_POST['openid_identifier'];
				}
        	# The following two lines request email, full name, and a nickname
        	# from the provider. Remove them if you don't need that data.
        	//$openid->required = array('contact/email');
				$openid->optional = array('namePerson', 'namePerson/friendly', 'contact/email');
				header('Location: ' . $openid->authUrl());
			}
			else if($openid->mode == 'cancel') {
			}
			else {
				foreach ($openid->getAttributes() as $key => $value) {
					if ($key == 'namePerson/friendly') {
						$alert = '';
						if (!user_store(strtoupper($value))) {
							$alert = '?alert=true';
						}
						$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
						header('Location: ' . $uri_parts[0] . $alert);
					}
				}
			}
		} catch(ErrorException $e) {

		}

		$output = <<<LOGIN
			<div style="padding-top:30px;padding-bottom:60px;text-align:center">
				<img src="includes/pic/joy.png" />
			</div>
			<form action="" method=POST>
				<input type="hidden" name="openid_identifier" value="https://openid.nus.edu.sg">
				<div style="text-align:center">
					<button class="btn btn-login"><span style="font-size:40px">Login with NUS OpenID</span></button>
				</div>
			</form>

LOGIN;

		return $output;
	}
}
?>