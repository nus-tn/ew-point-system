<?php
/**
* 		
*/
date_default_timezone_set('Asia/Shanghai');

require_once('includes/db.php');
require_once('includes/functions.php');
require_once('class/page.php');

class user extends page
{
	var $uid;
	var $user_name;
	var $matric_no;
	var $wid;
	var $mid;
	var $rid;
	var $email;
	var $room;
	var $contact;
	var $projects;
	/*
	Example format:
	array(
		"test_project1" => array(
			"project"=>"test1",
			"base_point"=>"10",
			"weightage"=>"0.5",
			"comments"=>"null",
		),
		"test_project2" => array(
			"project"=>"test2",
			"base_point"=>"20",
			"weightage"=>"0.6",
			"comments"=>"none",
		)
	);*/


	function __construct($uid) {
		$this->uid       = $uid;
		$this->matric_no = get_user_matric_no($uid);
		$this->user_name = get_user_name($uid);
		$this->wid       = get_user_wing($uid);
		$this->mid       = get_user_msw($uid);
		$this->rid       = get_user_role($uid);
		$this->email 	 = get_user_email($uid);
		$this->room 	 = get_user_room($uid);
		$this->contact 	 = get_user_contact($uid);
		$projects        = get_user_project($this->uid);
		if (!empty($projects)) {
			foreach ($projects as $key => $value) {
				$this->projects[] = get_project_record_info($value);
			}
		}
	}

	protected function log_out() {
		user_log_out();
		$uri = explode('?', $_SERVER['REQUEST_URI']);
		header('Location: '.$uri[0]);
	}

	public function display_menu() {
		// To be overwritten
	}
}


?>