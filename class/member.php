<?php
/**
* 		
*/
date_default_timezone_set('Asia/Shanghai');

require_once('includes/db.php');
require_once('class/user.php');

class member extends user
{
	
	function __construct($uid) {
		parent :: __construct($uid);
	}

	public function display() {
		if (isset($_GET['operation'])) {

			switch ($_GET['operation']) {

				case 'view-project':
						if (isset($_REQUEST['pid'])) {
							if (isset($_REQUEST['update-project'])) {
								$assignee = array();
								foreach (get_wing_member($this->wid) as $key => $value) {
									if (isset($_REQUEST["member-$value"]) && ($_REQUEST["member-$value"])) {
										$assignee[] = $value;
									}
								}
								if (update_project($_REQUEST['pid'],
												   $_REQUEST['profile-name'],
												   $_REQUEST['profile-point'],
												   $_REQUEST['profile-description'],
												   $assignee)) {
									//alert("project updated");
									$profile = new project_profile($_REQUEST['pid'], $this);
									$_SESSION['mode'] = 'project';
									return $profile->display();
								} else {
									alert("updating project failed");
									$_SESSION['mode'] = 'project';
								}
							} else if (isset($_REQUEST['delete-project'])){
								if (delete_project($_REQUEST['pid'])) {
									//alert("project deleted");
									$_SESSION['mode'] = 'project';
									return $this->display_view_projects($this->uid);
								} else {
									alert("deleting project failed");
									$_SESSION['mode'] = 'project';
								}
							} else if (isset($_REQUEST['start-project'])) {
								if (start_project($_REQUEST['pid'])) {
									//alert("project started");
									$profile = new project_profile($_REQUEST['pid'], $this);
									$_SESSION['mode'] = 'project';
									return $profile->display();
								} else {
									alert("starting project failed");
									$_SESSION['mode'] = 'project';
								}
							} else if (isset($_REQUEST['end-project'])){
								if (end_project($_REQUEST['pid'])) {
									//alert("project ended");
									$profile = new project_profile($_REQUEST['pid'], $this);
									$_SESSION['mode'] = 'project';
									return $profile->display();
								} else {
									alert("ending project failed");
									$_SESSION['mode'] = 'project';
								}
							} else {
								$profile = new project_profile($_REQUEST['pid'], $this);
								$_SESSION['mode'] = 'project';
								return $profile->display();
							}
						} else if (isset($_REQUEST['uid'])) {
							$_SESSION['mode'] = 'project';
							return $this->display_menu() . $this->display_view_project($_REQUEST['uid']);
						} else {
							$_SESSION['mode'] = 'project';
							return $this->display_menu() . $this->display_view_project($this->uid);
						}
						break;

				case 'view-others-project':
					$_SESSION['mode'] = 'project';
					return $this->display_menu() . $this->display_other_members_project();
					break;

				case 'view-member':
					if (isset($_REQUEST['uid'])) {
						if (isset($_REQUEST['update-member'])) {
							if (update_member($_REQUEST['profile-uid'], 
											  $_REQUEST['profile-wing'],
											  $_REQUEST['profile-msw'],
											  $_REQUEST['profile-email'], 
											  $_REQUEST['profile-room'], 
											  $_REQUEST['profile-contact'])) {
								//alert("member updated");
								$_SESSION['mode'] = 'member';
								$profile = new member_profile($_REQUEST['uid'], $this);
								return $profile->display();
							} else {
								alert("updating member failed");
								$_SESSION['mode'] = 'member';
								$profile = new member_profile($_REQUEST['uid'], $this);
								return $profile->display();
							}
						} else {
							$_SESSION['mode'] = 'member';
							$profile = new member_profile($_REQUEST['uid'], $this);
							return $profile->display();
						}
					} else {
						if (isset($_REQUEST['update-member'])) {
							if (update_member($this->uid, 
											  $_REQUEST['profile-wing'],
											  $_REQUEST['profile-msw'],
											  $_REQUEST['profile-email'], 
											  $_REQUEST['profile-room'], 
											  $_REQUEST['profile-contact'])) {
								//alert("member updated");
								$_SESSION['mode'] = 'member';
								$profile = new member_profile($this->uid, $this);
								return $profile->display();
							} else {
								alert("updating member failed");
								$_SESSION['mode'] = 'member';
								$profile = new member_profile($this->uid, $this);
								return $profile->display();
							}
						} else {
							$_SESSION['mode'] = 'member';
							$profile = new member_profile($this->uid, $this);
							return $profile->display();
						}
					}
					break;
				
				case 'view-others-profile':
					$_SESSION['mode'] = 'member';
					return $this->display_menu() . $this->display_other_members_profile();
					break;

				case 'log-out':
					return $this->log_out();
					break;

				default:
					# code...
					break;
			}
		} else {
			$_SESSION['mode'] = 'home';
			return $this->display_menu() . $this->display_member(); 
		}
	}

	/* 
	Display user information and user point
	*/
	private function display_member()
	{
		$wing_name   = get_wing_name($this->wid);
		$msw_name    = get_msw_name($this->mid);
		$total_point = get_user_total_point($this->uid);
		// $this->connect();
		// $this->buildDB();
		$output = <<<MEMBER_INFO
			<div>
				<h4 class="text-center">{$this->user_name}</h4>
			</div>
			<table class="table table-hover">
				<tr>
					<td>Matric Number</td>
					<td>{$this->matric_no}</td>
				</tr>
				<tr>
					<td>Specialization Wing</td>
					<td>$wing_name</td>
				</tr>
				<tr>
					<td>MSW</td>
					<td>$msw_name</td>
				</tr>
				<tr>
					<td>Total points</td> 
					<td>$total_point / 25</td> 
				</tr>
			</table>
MEMBER_INFO;
		return $output;
	}

	public function display_menu()	
	{	
		$rid = ROLE_HEAD;
		$home_active = '';
		$project_active = '';
		$member_active = '';
		$help_active = '';
		$switch = is_head($this->uid) ? "<li><a href='?switch_role=$rid'>Switch to head</a></li>" : '';
		if (!isset($_SESSION['mode'])) {
			$_SESSION['mode'] = 'home';
		}
		switch ($_SESSION['mode']) {
			case 'home':
				$home_active = 'active';
				break;
			
			case 'project':
				$project_active = 'active';
				break;

			case 'member':
				$member_active = 'active';
				break;

			case 'help':
				$help_active = 'active';
				break;

			default:
				# code...
				break;
		}
		return <<<MEMBER_MENU

		<ul class="nav nav-tabs" role="tablist">
			<li class="$home_active"><a href="?">Home</a></li>

			<li class="dropdown $project_active">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">
					Projects <span class="caret"></span>
				</a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="?operation=view-project">View My Projects</a></li>
					<li><a href="?operation=view-others-project">View Other's Projects</a></li>
				</ul>
			</li>

			<li class="dropdown $member_active">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">
					Members <span class="caret"></span>
				</a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="?operation=view-member">View My Profile</a></li>
					<li><a href="?operation=view-others-profile">View Other's Profile</a></li>
				</ul>
			</li>

			<!--<li><a class="$help_active" href="#">Help</a></li>-->

			$switch
			<li><a href="?operation=log-out">Log out</a></li>
		</ul>

MEMBER_MENU;
	}

	private function display_view_project($uid) {
		$name = get_user_name($uid);
		$table = <<<MEMBER_VIEW_PROJECT
			
			<div>
				<h4 class="text-center">Points allocation for $name</h4>
			</div>
			<table class="table table-hover">
				<tr>
					<th>Project</th>
					<!--<th>Base point</th>-->
					<!--<th>Weightage</th>-->
					<th>Points allocated</th>
					<th>Comments</th>
				</tr>
MEMBER_VIEW_PROJECT;

		$output = '';
		$pro = get_user_project($uid);
		$project_list = array();
		if (!empty($pro)) {
			foreach ($pro as $key => $value) {
				$project_list[] = get_project_record_info($value);
			}
		}
		if (count($pro) == 0) {
				$output .= <<<NO_PROJECT_MESSAGE
					<tr>
						<td colspan=5>
							<div style="text-align:center; color:#AAA">No assigned project found.</div>
						</td>
					</tr>
NO_PROJECT_MESSAGE;
		} else {
			foreach ($project_list as $project) {
				// $point = $project['base_point'] * $project['weightage'];
				$point = $project['rpoints'];
				$project_name = $project_name = '<a href="?operation=view-project&pid=' . $project['pid'] . '">' . $project['name'] . '</a>';
				$output .= <<<PROJECT
				<tr>
					<td>$project_name</td>
					<td>$point</td>
					<td>{$project['comments']}</td>
				</tr>
PROJECT;
			}
		}

		$total = get_user_total_point($uid);
		$close = <<<MEMBER_VIEW_PROJECT_CLOSE
		</table>
		<div>
			<h5 class="text-right">Total Points: $total</h5>
		</div>
MEMBER_VIEW_PROJECT_CLOSE;

		return $table . $output . $close;
	}
	
	private function display_other_members_profile() {
		//display the default admin page 
		$wtitle = FALSE;
		$wmembers = array();
		$wmembers_name = array();
		$wprojects = '';

		$mtitle = FALSE;
		$mmembers = array();
		$mmembers_name = array();
		$mprojects = '';

		if ($this->wid != WING_NULL) {
			$wprojects = get_wing_project($this->wid);
			$wmembers = get_wing_member($this->wid);
			$wtitle = get_wing_name($this->wid) . ' wing member roster';
		} 
		if ($this->mid != MSW_NULL) {
			$mprojects = get_msw_project($this->mid);
			$mmembers = get_msw_member($this->mid);
			$mtitle = get_msw_name($this->mid) . ' msw member roster';
		}

		$output = '';


		// Member List
		if ($wtitle) {
			$output .= <<<OTHER_WING_MEMBER_TITLE
			<div>
				<h4 class="text-center">$wtitle</h4>
			</div>
OTHER_WING_MEMBER_TITLE;

			foreach ($wmembers as $key => $value) {
				$wmembers_name[$key] = get_user_name($value);
				if (is_wing_head($value)) {
					$wmembers_name[$key] .= ' <span style="color:red">&#10040;</span>';
				}
			}
			for ($i = 1; $i <= count($wmembers) % 4; $i++) { 
				$wmembers_name[] = '';
				$wmembers[] = -1;
			}
			$output .= '<table class="table table-plain" style="text-align : center">';
			for ($i = 0; $i < count($wmembers) / 4 ; $i++) { 
				$output .=	<<<ADMIN_HOME_WING_MEMBER
					<tr>
						<td style="width : 25%"><a href='?operation=view-member&uid={$wmembers[$i * 4]}'>{$wmembers_name[$i * 4]}</a></td>
						<td style="width : 25%"><a href='?operation=view-member&uid={$wmembers[$i * 4 + 1]}'>{$wmembers_name[$i * 4 + 1]}</a></td>
						<td style="width : 25%"><a href='?operation=view-member&uid={$wmembers[$i * 4 + 2]}'>{$wmembers_name[$i * 4 + 2]}</a></td>
						<td style="width : 25%"><a href='?operation=view-member&uid={$wmembers[$i * 4 + 3]}'>{$wmembers_name[$i * 4 + 3]}</a></td>
					</tr>
ADMIN_HOME_WING_MEMBER;
			}
			$output .= '</table><br /><br />';
		}

		if ($mtitle) {
			$output .= <<<OTHER_MSW_MEMBER_TITLE
			<div>
				<h4 class="text-center">$mtitle</h4>
			</div>
OTHER_MSW_MEMBER_TITLE;

			foreach ($mmembers as $key => $value) {
				$mmembers_name[$key] = get_user_name($value);
				if (is_msw_head($value)) {
					$mmembers_name[$key] .= ' <span style="color:red">&#10040;</span>';
				}
			}
			for ($i = 1; $i <= count($mmembers) % 4; $i++) { 
				$mmembers_name[] = '';
				$mmembers[] = -1;
			}
			$output .= '<table class="table table-plain" style="text-align : center">';
			for ($i = 0; $i < count($mmembers) / 4 ; $i++) { 
				$output .=	<<<ADMIN_HOME_MSW_MEMBER
					<tr>
						<td style="width : 25%"><a href='?operation=view-member&uid={$mmembers[$i * 4]}'>{$mmembers_name[$i * 4]}</a></td>
						<td style="width : 25%"><a href='?operation=view-member&uid={$mmembers[$i * 4 + 1]}'>{$mmembers_name[$i * 4 + 1]}</a></td>
						<td style="width : 25%"><a href='?operation=view-member&uid={$mmembers[$i * 4 + 2]}'>{$mmembers_name[$i * 4 + 2]}</a></td>
						<td style="width : 25%"><a href='?operation=view-member&uid={$mmembers[$i * 4 + 3]}'>{$mmembers_name[$i * 4 + 3]}</a></td>
					</tr>
ADMIN_HOME_MSW_MEMBER;
			}
			$output .= '</table>';
		}



		return $output;
	}

	private function display_other_members_project() {
		//display the default admin page 
		$wtitle = FALSE;
		$wmembers = array();
		$wmembers_name = array();
		$wprojects = '';

		$mtitle = FALSE;
		$mmembers = array();
		$mmembers_name = array();
		$mprojects = '';

		if ($this->wid != WING_NULL) {
			$wprojects = get_wing_project($this->wid);
			$wmembers = get_wing_member($this->wid);
			$wtitle = get_wing_name($this->wid) . ' wing member roster';
		} 
		if ($this->mid != MSW_NULL) {
			$mprojects = get_msw_project($this->mid);
			$mmembers = get_msw_member($this->mid);
			$mtitle = get_msw_name($this->mid) . ' msw member roster';
		}

		$output = '';


		// Member List
		if ($wtitle) {
			$output .= <<<OTHER_WING_MEMBER_TITLE
			<div>
				<h4 class="text-center">$wtitle</h4>
			</div>
OTHER_WING_MEMBER_TITLE;

			foreach ($wmembers as $key => $value) {
				$wmembers_name[$key] = get_user_name($value);
				if (is_wing_head($value)) {
					$wmembers_name[$key] .= ' <span style="color:red">&#10040;</span>';
				}
			}
			for ($i = 1; $i <= count($wmembers) % 4; $i++) { 
				$wmembers_name[] = '';
				$wmembers[] = -1;
			}
			$output .= '<table class="table table-plain" style="text-align : center">';
			for ($i = 0; $i < count($wmembers) / 4 ; $i++) { 
				$output .=	<<<ADMIN_HOME_WING_MEMBER
					<tr>
						<td style="width : 25%"><a href='?operation=view-project&uid={$wmembers[$i * 4]}'>{$wmembers_name[$i * 4]}</a></td>
						<td style="width : 25%"><a href='?operation=view-project&uid={$wmembers[$i * 4 + 1]}'>{$wmembers_name[$i * 4 + 1]}</a></td>
						<td style="width : 25%"><a href='?operation=view-project&uid={$wmembers[$i * 4 + 2]}'>{$wmembers_name[$i * 4 + 2]}</a></td>
						<td style="width : 25%"><a href='?operation=view-project&uid={$wmembers[$i * 4 + 3]}'>{$wmembers_name[$i * 4 + 3]}</a></td>
					</tr>
ADMIN_HOME_WING_MEMBER;
			}
			$output .= '</table><br /><br />';
		}

		if ($mtitle) {
			$output .= <<<OTHER_MSW_MEMBER_TITLE
			<div>
				<h4 class="text-center">$mtitle</h4>
			</div>
OTHER_MSW_MEMBER_TITLE;

			foreach ($mmembers as $key => $value) {
				$mmembers_name[$key] = get_user_name($value);
				if (is_msw_head($value)) {
					$mmembers_name[$key] .= ' <span style="color:red">&#10040;</span>';
				}
			}
			for ($i = 1; $i <= count($mmembers) % 4; $i++) { 
				$mmembers_name[] = '';
				$mmembers[] = -1;
			}
			$output .= '<table class="table table-plain" style="text-align : center">';
			for ($i = 0; $i < count($mmembers) / 4 ; $i++) { 
				$output .=	<<<ADMIN_HOME_MSW_MEMBER
					<tr>
						<td style="width : 25%"><a href='?operation=view-project&uid={$mmembers[$i * 4]}'>{$mmembers_name[$i * 4]}</a></td>
						<td style="width : 25%"><a href='?operation=view-project&uid={$mmembers[$i * 4 + 1]}'>{$mmembers_name[$i * 4 + 1]}</a></td>
						<td style="width : 25%"><a href='?operation=view-project&uid={$mmembers[$i * 4 + 2]}'>{$mmembers_name[$i * 4 + 2]}</a></td>
						<td style="width : 25%"><a href='?operation=view-project&uid={$mmembers[$i * 4 + 3]}'>{$mmembers_name[$i * 4 + 3]}</a></td>
					</tr>
ADMIN_HOME_MSW_MEMBER;
			}
			$output .= '</table>';
		}

		return $output;
	}
}

?>