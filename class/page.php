<?php
/**
* 		
*/
require_once("includes/functions.php");

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

class page
{	
	public function __construct() {
		$_SESSION['mode'] = 'home';
	}

	public function display_header() {
		$date_year = date("Y");
		$date_month= date("F");
		if($date_month>=7)
			$year = $date_year;
		else
			$year = $date_year-1;	
		$next_year = $year + 1;	

		return <<<HEADER

		<!DOCTYPE HTML>

		<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<script src="http://code.jquery.com/jquery-2.0.0.js"></script>
			<script src="includes/js/bootstrap.min.js"></script>
			<title>EW Point System</title>
		    <link href="includes/css/bootstrap.min.css" rel="stylesheet">
		    <link href="includes/css/bootstrap-theme.css" rel="stylesheet">
			<script>
				$(function(){
					$(".project-title").click(function(e){
						e.preventDefault();
						console.log(e.target.id);
						$("#project-details-" + e.target.id).slideToggle();
					});
					console.log($(".td-options").width());
					$(".td-options").width($(".update").width() + $(".update").width() + 72);
				});
			</script>
		</head>
		<body>
		
			<div class="container wrapper">
			<div class="content">
				<div style="padding-top: 20px;padding-bottom: 20px;">
					<p style="font-size: 58px" class="text-center" >
						<b>EusoffWorks Points System</b>
					</p>
				</div>

HEADER;
	}

	public function display_footer() {
		return <<<FOOTER
		</div>
		<div class="footer text-center">
		<p>Copyright &#169; 2015 EusoffWorks</p>
		</div>

		</div>

		</body>
		</html>
FOOTER;
	}

}
?>