UPDATE `record` r,
(  
    SELECT pid, ppoint as points
    FROM `project`  
) p
SET r.`weightage` = ROUND(p.points * r.weightage * 10) / 10
WHERE r.pid = p.pid

ALTER TABLE `record` ADD `rpoints` DOUBLE;
UPDATE `record` SET `rpoints` = `weightage`;
ALTER TABLE `record` DROP COLUMN `weightage`;

UPDATE `project` p,
(
	SELECT SUM(`rpoints`) as points, pid
	FROM `record`
	GROUP BY `pid`
) r
SET p.`ppoint` = r.`points`
WHERE p.`pid` = r.`pid`