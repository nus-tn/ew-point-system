<?php
	require_once("constants.inc");
	require_once("functions.php");

	/**
	 * db_connect: connect to database
	 * @return the link of databse
	 */
	function db_connect() {
		$con = mysql_connect(DB_HOST,DB_USERNAME,DB_PWD);
		if (!$con)
		{   
			die('Could not connect: ' . mysql_error());
		}
		return $con;
	}

	// ---------------user---------------
	function is_ew_member($uid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT * FROM " . USER_TABLE . " WHERE `uid` = $uid AND `deleted` = 0";
		$result = mysql_query($query);
		mysql_close($con);
		
		if (empty($result)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * get_user_role
	 * @param uid
	 * @return int role id, -1 if user does not exist
	 */
	function get_user_role($uid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `rid` FROM " . USER_TABLE . " WHERE `uid` = $uid AND `deleted` = 0";
		$result = mysql_query($query);

		if ($result) {
			$result = mysql_fetch_array($result);
		} else {
			mysql_close($con);
			return NULL;
		}

		mysql_close($con);
		if ($result) {
			return $result['rid'];
		} else {
			return -1;
		}
	}

	/**
	 * get_user_msw
	 * @param uid
	 * @return int msw id, -1 if user does not exist
	 */
	function get_user_msw($uid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `mid` FROM " . USER_TABLE . " WHERE `uid` = $uid AND `deleted` = 0";
		$result = mysql_query($query);

		if ($result) {
			$result = mysql_fetch_array($result);
		} else {
			mysql_close($con);
			return NULL;
		}

		mysql_close($con);
		if ($result) {
			return $result['mid'];
		} else {
			return -1;
		}
	}

	/**
	 * get_user_name
	 * @param uid
	 * @return user name, NULL if user does not exist
	 */
	function get_user_name($uid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `name` FROM " . USER_TABLE . " WHERE `uid` = $uid AND `deleted` = 0";
		$result = mysql_query($query);
		if ($result) {
			$result = mysql_fetch_array($result);
		} else {
			return NULL;
		}
		mysql_close($con);
		if ($result) {
			return $result['name'];
		} else {
			return NULL;
		}
	}

	/**
	 * get_user_matric_no
	 * @param uid
	 * @return matric no
	 */
	function get_user_matric_no($uid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `matric` FROM " . USER_TABLE . " WHERE `uid` = $uid AND `deleted` = 0";
		$result = mysql_query($query);
		if ($result) {
			$result = mysql_fetch_array($result);
		} else {
			mysql_close($con);
			return NULL;
		}
		mysql_close($con);
		if ($result) {
			return $result['matric'];
		} else {
			return NULL;
		}
	}

	/**
	 * get_user_wing
	 * @param uid
	 * @return int wing id, -1 if user does not exist
	 */
	function get_user_wing($uid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `wid` FROM " . USER_TABLE . " WHERE `uid` = $uid AND `deleted` = 0";
		$result = mysql_query($query);
		if ($result) {
			$result = mysql_fetch_array($result);
		} else {
			return -1;
		}
		mysql_close($con);
		if ($result) {
			return $result['wid'];
		} else {
			return -1;
		}
	}

	/**
	 * get_user_wing_name
	 * @param uid
	 * @return wing name
	 */
	function get_user_wing_name($uid) {
		$wid = get_user_wing($uid);

		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `wname` FROM " . WING_TABLE . " WHERE `wid` = $wid";
		$result = mysql_query($query);
		if ($result) {
			$result = mysql_fetch_array($result);
		} else {
			mysql_close($con);
			return NULL;
		}

		mysql_close($con);
		if ($result) {
			return $result['wname'];
		} else {
			return NULL;
		}
	}

	/**
	 * get_user_email
	 * @param uid
	 * @return user email, NULL if user does not exist
	 */
	function get_user_email($uid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `email` FROM " . USER_TABLE . " WHERE `uid` = $uid AND `deleted` = 0";
		$result = mysql_query($query);
		if ($result) {
			$result = mysql_fetch_array($result);
		} else {
			return NULL;
		}
		mysql_close($con);
		if ($result) {
			return $result['email'];
		} else {
			return NULL;
		}
	}

	/**
	 * get_user_room
	 * @param uid
	 * @return user room, NULL if user does not exist
	 */
	function get_user_room($uid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `room` FROM " . USER_TABLE . " WHERE `uid` = $uid AND `deleted` = 0";
		$result = mysql_query($query);
		if ($result) {
			$result = mysql_fetch_array($result);
		} else {
			return NULL;
		}
		mysql_close($con);
		if ($result) {
			return $result['room'];
		} else {
			return NULL;
		}
	}

	/**
	 * get_user_contact
	 * @param uid
	 * @return user contact, NULL if user does not exist
	 */
	function get_user_contact($uid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `contact` FROM " . USER_TABLE . " WHERE `uid` = $uid AND `deleted` = 0";
		$result = mysql_query($query);
		if ($result) {
			$result = mysql_fetch_array($result);
		} else {
			return NULL;
		}
		mysql_close($con);
		if ($result) {
			return $result['contact'];
		} else {
			return NULL;
		}
	}

	/**
	 * get_user_project
	 * @param uid
	 * @return an array of record id
	 */
	function get_user_project($uid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);
		$projects = array();

		$query = "SELECT `rcid` FROM " . RECORD_TABLE . " WHERE `uid` = $uid  AND `deleted` = 0";
		$result = mysql_query($query);
		if ($result) {
			while ($row = mysql_fetch_array($result)) {
				$projects[] = $row['rcid'];
			}
			return $projects;
		} else {
			return FALSE;
		}
		mysql_close($con);
	}

	/**
	 * get_user_id_by_matric_no
	 * @param matric_no
	 * @return user id, -1 if not found
	 */
	function get_user_id_by_matric_no($matric_no) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `uid` FROM " . USER_TABLE . " WHERE `matric` = '$matric_no' AND `deleted` = 0";
		$result = mysql_query($query);

		if ($result) {
			$result = mysql_fetch_array($result);
		} else {
			return NULL;
		}
		mysql_close($con);
		if ($result) {
			return $result['uid'];
		} else {
			return NULL;
		}
	}

	/**
	 * is_wing_head: check whether a user is wing head
	 * @param uid
	 * @return boolean
	 */
	function is_wing_head($uid) {
		return get_user_role($uid) == ROLE_WING_HEAD;
	}

	/**
	 * is_msw_head: check whether a user is wing head
	 * @param uid
	 * @return boolean
	 */
	function is_msw_head($uid) {
		return get_user_role($uid) == ROLE_MSW_HEAD;
	}

	/**
	 * is_head: check whether a user is head
	 * @param uid
	 * @return boolean
	 */
	function is_head($uid) {
		return is_wing_head($uid) || is_msw_head($uid);
	}

	/**
	 * is_member: check whether a user is normal member
	 * @param uid
	 * @return boolean
	 */
	function is_member($uid) {
		return get_user_role($uid) == ROLE_MEMBER;
	}


	// ---------------project---------------
	
	/**
	 * get_project_status
	 * @param pid
	 * @return project status
	 */
	function get_project_status($pid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `pstatus` FROM " . PROJECT_TABLE . " WHERE `pid` = $pid AND `deleted` = 0";
		$result = mysql_query($query);
		$result = mysql_fetch_array($result);
		mysql_close($con);
		if ($result) {
			return $result['pstatus'];
		} else {
			return FALSE;
		}
	}

	/**
	 * get_project_name
	 * @param pid
	 * @return project name
	 */
	function get_project_name($pid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `pname` FROM " . PROJECT_TABLE . " WHERE `pid` = $pid AND `deleted` = 0";
		$result = mysql_query($query);
		$result = mysql_fetch_array($result);
		mysql_close($con);
		if ($result) {
			return $result['pname'];
		} else {
			return FALSE;
		}
	}

	/**
	 * get_project_wing
	 * @param pid
	 * @return project wid
	 */
	function get_project_wing($pid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `wid` FROM " . PROJECT_TABLE . " WHERE `pid` = $pid AND `deleted` = 0";
		$result = mysql_query($query);
		$result = mysql_fetch_array($result);
		mysql_close($con);
		if ($result) {
			return $result['wid'];
		} else {
			return FALSE;
		}
	}

	/**
	 * get_project_msw
	 * @param pid
	 * @return project mid
	 */
	function get_project_msw($pid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `mid` FROM " . PROJECT_TABLE . " WHERE `pid` = $pid AND `deleted` = 0";
		$result = mysql_query($query);
		$result = mysql_fetch_array($result);
		mysql_close($con);
		if ($result) {
			return $result['mid'];
		} else {
			return FALSE;
		}
	}

	/**
	 * get_project_base_point
	 * @param pid
	 * @return project base point
	 */
	function get_project_base_point($pid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `ppoint` FROM " . PROJECT_TABLE . " WHERE `pid` = $pid AND `deleted` = 0";
		$result = mysql_query($query);
		if ($result) {
			$result = mysql_fetch_array($result);
			mysql_close($con);
			return $result['ppoint'];
		} else {
			mysql_close($con);
			return FALSE;
		}
	}

	/**
	 * get_project_description
	 * @param pid
	 * @return project description
	 */
	function get_project_description($pid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `pdescription` FROM " . PROJECT_TABLE . " WHERE `pid` = $pid AND `deleted` = 0";
		$result = mysql_query($query);
		$result = mysql_fetch_array($result);
		mysql_close($con);
		if ($result) {
			return $result['pdescription'];
		} else {
			return FALSE;
		}
	}

	/**
	 * get_project_assignee
	 * @param pid
	 * @return an array of uid
	 */
	function get_project_assignee($pid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$assignee = array();
		$query = "SELECT `uid` FROM " . RECORD_TABLE . " WHERE `pid` = $pid AND `deleted` = 0";
		$result = mysql_query($query);
		while ($row = mysql_fetch_array($result)) {
			$assignee[] = $row['uid'];
		}

		return $assignee;
	}

	/**
	 * get_project_rcid
	 * @param pid
	 * @return rcid
	 */
	function get_project_rcid($pid) {
		$assignee = get_project_assignee($pid);
		foreach ($assignee as $key => $value) {
			$assignee[$key] = get_record_id($pid, $value);
		}
		return $assignee;
	}

	/**
	 * get_project_info
	 * @param $pid
	 * @return array
	 */
	function get_project_info($pid) {
		return array(
				"name"       => get_project_name($pid),
				"base_point" => get_project_base_point($pid),
				"description" => get_project_description($pid),
				"status"   => get_project_status($pid),
				"assignee" => get_project_assignee($pid)
			);
	}

	/**
	 * add_project: create a project
	 * @param pname project name
	 * @param ppoint base point for this project (total points for allocation)
	 * @param pwing wing id
	 * @param pmsw msw id
	 * @param pdescription (optional) project description
	 * @param pstatus status for this project (optional, by default: not started)
	 * @return boolean whether creating is successful
	 */
	function add_project($pname, $ppoint, $pwing, $pmsw, $pdescription = NULL, $pstatus = PROJECT_STATUS_NOT_STARTED) {
		$current_user = current_user();
		if (!is_head($current_user) && !is_admin($current_user)) {
			throw new Exception('User have no right to create a project');
		} else {
			if ((!is_numeric($ppoint)) || ($ppoint <= 0)) {
				throw new Exception('base point must be positive number');
			} 

			$con = db_connect();
			mysql_select_db(DB_NAME, $con);

			$query = "INSERT INTO " . PROJECT_TABLE . "(wid, mid, pname, pdescription, ppoint, pstatus, deleted) VALUES ($pwing, $pmsw, '$pname', '$pdescription', $ppoint, '$pstatus', 0)";
			$result = mysql_query($query);

			mysql_close($con);

			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	/**
	 * update_project: update a project
	 * @param pid project id
	 * @param pname project name
	 * @param ppoint base point for this project
	 * @param pdescription (optional) project description
	 * @param passignee array of assignee's uid
	 * @return boolean whether creating is successful
	 */
	function update_project($pid, $pname, $ppoint, $pdescription, $passignee) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "UPDATE " . PROJECT_TABLE . " SET `pname` = '$pname', `ppoint` = $ppoint, `pdescription` = '$pdescription' WHERE `pid` = $pid";

		$result = mysql_query($query);

		mysql_close($con);

		// Transaction to be implemented
		// print_r($passignee);
		foreach ($passignee as $key => $value) {
			assign_project($pid, $value);
		}

		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * start_project: start a project
	 * @param pid project id
	 * @return boolean
	 */
	function start_project($pid) {
		$current_user = current_user();
		if (!is_head($current_user) && !is_admin($current_user)) {
			throw new Exception('User have no right to start a project');
		} else {
			$status = get_project_status($pid);
			$con = db_connect();
			mysql_select_db(DB_NAME, $con);
			$query = "UPDATE " . PROJECT_TABLE . " SET `pstatus` = '" . PROJECT_STATUS_IN_PROGRESS . "' WHERE `pid` = $pid";
			$result = mysql_query($query);
			mysql_close($con);
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	/**
	 * end_project: end a project
	 * @param pid project id
	 * @return boolean
	 */
	function end_project($pid) {
		$current_user = current_user();
		if (!is_head($current_user) && !is_admin($current_user)) {
			throw new Exception('User have no right to end a project');
		} else {
			$status = get_project_status($pid);
			$con = db_connect();
			mysql_select_db(DB_NAME, $con);
			$query = "UPDATE " . PROJECT_TABLE . " SET `pstatus` = '" . PROJECT_STATUS_FINISHED . "' WHERE `pid` = $pid";
			$result = mysql_query($query);
			mysql_close($con);
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	/**
	 * delete_project(
	 * @param $uid
	 */
	function delete_project($pid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "UPDATE " . PROJECT_TABLE . " SET `deleted` = 1 WHERE `pid` = $pid";
		$result1 = mysql_query($query);

		$query = "UPDATE " . RECORD_TABLE . " SET `deleted` = 1 WHERE `pid` = $pid";
		$result2 = mysql_query($query);
 	
		mysql_close($con);

		if ($result1 && $result2) {
			return TRUE;
		} else {
			return FALSE;
		}
	}


	// ---------------record---------------

	/**
	 * get_project_record_info
	 * @param $rid
	 * @return array
	 */
	function get_project_record_info($rcid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT * FROM " . RECORD_TABLE . " WHERE `rcid` = $rcid AND `deleted` = 0";
		$result = mysql_query($query);
		if ($result) {
			$result = mysql_fetch_array($result);
		} else {
			mysql_close($con);
			return -1;
		}
		mysql_close($con);
		if ($result) {
			return array(
					"pid"        => $result['pid'],
					"name"       => get_project_name($result['pid']),
					"base_point" => get_project_base_point($result['pid']),
					// "weightage"  => $result['weightage'],
					"rpoints"    => $result['rpoints'],
					"comments"   => $result['comments']
				);
		} else {
			return -1;
		}
	}

	/**
	 * get_record_uid
	 * @param rcid
	 * @return uid
	 */
	function get_record_uid($rcid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `uid` FROM " . RECORD_TABLE . " WHERE `rcid` = $rcid AND `deleted` = 0";
		$result = mysql_query($query);
		$result = mysql_fetch_array($result);
		mysql_close($con);
		if ($result) {
			return $result['uid'];
		} else {
			return FALSE;
		}
	}

	/**
	 * get_record_pid
	 * @param rcid
	 * @return pid
	 */
	function get_record_pid($rcid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `pid` FROM " . RECORD_TABLE . " WHERE `rcid` = $rcid AND `deleted` = 0";
		$result = mysql_query($query);
		if ($result) {
			$result = mysql_fetch_array($result);
			mysql_close($con);
			return $result['pid'];
		} else {
			mysql_close($con);
			return FALSE;
		}
	}

	/**
	 * get_record_id
	 * @param $pid
	 * @param $uid
	 * @return $rcid
	 */
	function get_record_id($pid, $uid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `rcid` FROM " . RECORD_TABLE . " WHERE `pid` = $pid AND `uid` = $uid AND `deleted` = 0";
		$result = mysql_query($query);
		$result = mysql_fetch_array($result);
		mysql_close($con);
		if ($result) {
			return $result['rcid'];
		} else {
			return FALSE;
		}
	}

	/**
	 * get_record_comments
	 * @param rcid
	 * @return comments
	 */
	function get_record_comments($rcid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `comments` FROM " . RECORD_TABLE . " WHERE `rcid` = $rcid AND `deleted` = 0";
		$result = mysql_query($query);
		if ($result) {
			$result = mysql_fetch_array($result);
			mysql_close($con);
			return $result['comments'];
		} else {
			mysql_close($con);
			return FALSE;
		}
	}

	/**
	 * get_record_weightage
	 * @param rcid
	 * @return weightage
	 * @deprecated
	 */
	function get_record_weightage($rcid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `weightage` FROM " . RECORD_TABLE . " WHERE `rcid` = $rcid AND `deleted` = 0";
		$result = mysql_query($query);
		if ($result) {
			$result = mysql_fetch_array($result);
			mysql_close($con);
			return $result['weightage'];
		} else {
			mysql_close($con);
			return FALSE;
		}
	}

	/**
	 * get_record_points
	 * @param rcid
	 * @return points
	 */
	// function get_record_points($rcid) {
	// 	return get_project_base_point(get_record_pid($rcid)) * get_record_weightage($rcid);
	// }
	function get_record_points($rcid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `rpoints` FROM " . RECORD_TABLE . " WHERE `rcid` = $rcid AND `deleted` = 0";
		$result = mysql_query($query);
		if ($result) {
			$result = mysql_fetch_array($result);
			mysql_close($con);
			return $result['rpoints'];
		} else {
			mysql_close($con);
			return FALSE;
		}
	}

	/**
	 * assign_project
	 * @param $pid
	 * @param $uid
	 * @param $rpoints
	 * @param $comments
	 */
	function assign_project($pid, $uid, $rpoints = 0, $comments = NULL) {
		$current_user = current_user();
		if (!is_head($current_user) && !is_admin($current_user)) {
			throw new Exception('User have no right to assign a project');
		} else {
			$con = db_connect();
			mysql_select_db(DB_NAME, $con);

			$query = "INSERT INTO " . RECORD_TABLE . "(pid, uid, rpoints, comments, deleted) VALUES ($pid, $uid, $rpoints, '$comments', 0)";
			$result = mysql_query($query);

			mysql_close($con);

			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	/**
	 * update_project_record
	 * @param $rcid
	 * @param $rpoints
	 * @param $comments
	 */
	function update_project_record($rcid, $rpoints = 0, $comments = NULL) {
		$current_user = current_user();
		if (!is_head($current_user) && !is_admin($current_user)) {
			throw new Exception('User have no right to update a record');
		} else {
			$con = db_connect();
			mysql_select_db(DB_NAME, $con);

			$query = "UPDATE " . RECORD_TABLE . " SET `rpoints` = $rpoints, `comments` = '$comments' WHERE `rcid` = $rcid";
			$result = mysql_query($query);

			mysql_close($con);

			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	/**
	 * delete_project_record
	 * @param $rcid
	 */
	function delete_project_record($rcid) {
		$current_user = current_user();
		if (!is_head($current_user) && !is_admin($current_user)) {
			throw new Exception('User have no right to update a record');
		} else {
			$con = db_connect();
			mysql_select_db(DB_NAME, $con);

			$query = "UPDATE " . RECORD_TABLE . " SET `deleted` = 1 WHERE `rcid` = $rcid";
			$result = mysql_query($query);

			mysql_close($con);

			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// ---------------admin---------------
	
	/**
	 * is_admin
	 * @param $uid
	 */
	function is_admin($uid) {
		return get_user_role($uid) == ROLE_ADMIN;
	}

	/**
	 * remove_member
	 * @param $uid
	 * @return boolean
	 */
	function remove_member($uid) {
		$current_user = current_user();
		if (!is_head($current_user) && !is_admin($current_user)) {
			throw new Exception('User have no right to add a member');
		} else {
			$con = db_connect();
			mysql_select_db(DB_NAME, $con);

			$query = "UPDATE " . USER_TABLE . " SET `deleted` = 1 WHERE `uid` = $uid";
			$result = mysql_query($query);

			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	/**
	 * add_member
	 * @param $matric
	 * @param $name
	 * @param $rid role id
	 * @param $wid wing id
	 * @param $mid msw id
	 * @param $email email address
	 * @param $room room no.
	 * @param $contact 
	 */
	function add_member($matric, $name, $rid, $wid, $mid, $email, $room, $contact) {

		$current_user = current_user();
		if (!is_head($current_user) && !is_admin($current_user)) {
			throw new Exception('User have no right to add a member');
		} else {
			$con = db_connect();
			mysql_select_db(DB_NAME, $con);

			$name = strtoupper($name);
			$query = "INSERT INTO " . USER_TABLE . " (matric, name, rid, wid, mid, email, room, contact, deleted) VALUES ('$matric', '$name', $rid, $wid, $mid, '$email', '$room', '$contact', 0)";
			$result = mysql_query($query);

			mysql_close($con);

			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	/**
	 * update_member
	 * @param $uid
	 * @param $wing
	 * @param $msw
	 * @param $email email address
	 * @param $room room no.
	 * @param $contact 
	 */
	function update_member($uid, $wing, $msw, $email, $room, $contact) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "UPDATE " . USER_TABLE . " SET `email` = '$email', `wid`=$wing, `mid`=$msw, `room` = '$room', `contact` = '$contact' WHERE `uid` = $uid";
		$result = mysql_query($query);

		mysql_close($con);

		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * delete_member(
	 * @param $uid
	 */
	function delete_member($uid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "UPDATE " . USER_TABLE . " SET `deleted` = 1 WHERE `uid` = $uid";
		$result1 = mysql_query($query);

		$query = "UPDATE " . RECORD_TABLE . " SET `deleted` = 1 WHERE `uid` = $uid";
		$result2 = mysql_query($query);

		mysql_close($con);

		if ($result1 && $result2) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	// ---------------wing---------------

	/**
	 * get_wing_name
	 * @param $wid
	 * @return wing name
	 */
	function get_wing_name($wid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `wname` FROM " . WING_TABLE . " WHERE `wid` = $wid";
		$result = mysql_query($query);
		if ($result) {
			$result = mysql_fetch_array($result);
		} else {
			return NULL;
		}
		mysql_close($con);
		if ($result) {
			return $result['wname'];
		} else {
			return NULL;
		}
	}

	/**
	 * get_wing_project
	 * @param wid
	 * @return an array of project id
	 */
	function get_wing_project($wid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);
		$projects = array();

		$query = "SELECT `pid` FROM " . PROJECT_TABLE . " WHERE `wid` = $wid  AND `deleted` = 0";
		$result = mysql_query($query);
		if ($result) {
			while ($row = mysql_fetch_array($result)) {
				$projects[] = $row['pid'];
			}
			return $projects;
		} else {
			return FALSE;
		}
		mysql_close($con);
	}

	function get_wing_unfinished_project($wid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);
		$projects = array();

		$query = "SELECT `pid` FROM " . PROJECT_TABLE . " WHERE `wid` = $wid  AND `deleted` = 0 AND `pstatus` != 'finished'";
		$result = mysql_query($query);
		if ($result) {
			while ($row = mysql_fetch_array($result)) {
				$projects[] = $row['pid'];
			}
			return $projects;
		} else {
			return FALSE;
		}
		mysql_close($con);
	}

	/**
	 * get_wing_member
	 * @param $wid, 0 for all
	 * @return an array of wing members' uid
	 */
	function get_wing_member($wid) {
		$members = array();
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		if ($wid < 0) {
			mysql_close($con);
			return $members;
		}

		if ($wid == 0) {
			$query = "SELECT * FROM " . USER_TABLE . " WHERE `deleted` = 0 AND `matric` NOT LIKE 'ADMIN%'";
		} else {
			$query = "SELECT * FROM " . USER_TABLE . " WHERE `wid` = $wid AND `deleted` = 0 AND `matric` NOT LIKE 'ADMIN%'";
		}
		$result = mysql_query($query);
		if ($result) {
			mysql_close($con);
			while ($row = mysql_fetch_array($result)) {
				$members[] = $row['uid'];
			}
		}

		return $members;
	}

	// ---------------msw---------------

	/**
	 * get_msw_name
	 * @param $mid
	 * @return msw name
	 */
	function get_msw_name($mid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		$query = "SELECT `mname` FROM " . MSW_TABLE . " WHERE `mid` = $mid";
		$result = mysql_query($query);
		if ($result) {
			$result = mysql_fetch_array($result);
		} else {
			return NULL;
		}
		mysql_close($con);
		if ($result) {
			return $result['mname'];
		} else {
			return NULL;
		}
	}

	/**
	 * get_msw_project
	 * @param mid
	 * @return an array of project id
	 */
	function get_msw_project($mid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);
		$projects = array();

		$query = "SELECT `pid` FROM " . PROJECT_TABLE . " WHERE `mid` = $mid OR `mid` = " . MSW_CROSS . "  AND `deleted` = 0";
		$result = mysql_query($query);
		if ($result) {
			while ($row = mysql_fetch_array($result)) {
				$projects[] = $row['pid'];
			}
			return $projects;
		} else {
			return FALSE;
		}
		mysql_close($con);
	}

	function get_msw_unfinished_project($mid) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);
		$projects = array();

		$query = "SELECT `pid` FROM " . PROJECT_TABLE . " WHERE `mid` = $mid OR `mid` = " . MSW_CROSS . "  AND `deleted` = 0 AND `pstatus` != 'finished'";
		$result = mysql_query($query);
		if ($result) {
			while ($row = mysql_fetch_array($result)) {
				$projects[] = $row['pid'];
			}
			return $projects;
		} else {
			return FALSE;
		}
		mysql_close($con);
	}

	function update_msw_member($mid, $members) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);
		$result = TRUE;

		// transaction needed		
		foreach ($members as $key => $value) {
			$query = "UPDATE " . USER_TABLE . " SET `mid` = $mid WHERE `uid` = $value";
			$result = mysql_query($query) && $result;
		}

		mysql_close($con);

		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * get_msw_member
	 * @param $mid, 0 for all
	 * @return an array of msw members' uid
	 */
	function get_msw_member($mid) {
		$members = array();
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);

		if ($mid < 0) {
			mysql_close($con);
			return $members;
		}

		if ($mid == 0) {
			$query = "SELECT * FROM " . USER_TABLE . " WHERE `deleted` = 0 AND `matric` NOT LIKE 'ADMIN%'";
		} else if ($mid == MSW_CROSS) {
			$query = "SELECT * FROM " . USER_TABLE . " WHERE `wid` = " . WING_PHOTO ." OR `wid` = " . WING_VIDEO . " AND `deleted` = 0 AND `matric` NOT LIKE 'ADMIN%'";
		} else {
			$query = "SELECT * FROM " . USER_TABLE . " WHERE `mid` = $mid AND `deleted` = 0 AND `matric` NOT LIKE 'ADMIN%'";
		}
		$result = mysql_query($query);
		if ($result) {
			mysql_close($con);
			while ($row = mysql_fetch_array($result)) {
				$members[] = $row['uid'];
			}
		}

		return $members;
	}

	function is_valid_id($id, $mode) {
		$con = db_connect();
		mysql_select_db(DB_NAME, $con);
		$projects = array();

		$table = '';
		$the_id = '';
		switch ($mode) {
			case 'project':
				$table = PROJECT_TABLE;
				$the_id = 'pid';
				break;

			case 'member':
				$table = USER_TABLE;
				$the_id = 'uid';
				break;

			case 'record':
				$table = RECORD_TABLE;
				$the_id = 'rcid';
				break;
			
			default:
				# code...
				break;
		}

		$query = "SELECT * FROM " . $table . " WHERE `$the_id` = $id AND `deleted` = 0";
		$result = mysql_query($query);
		$result = mysql_fetch_array($result);
		
		if ($result) {
			mysql_close($con);
			return TRUE;
		} else {
			mysql_close($con);
			return FALSE;
		}
	}


	function get_all_wings() {
		return array(2, 3, 4, 5);
	}

	function get_all_msws() {
		return array(2, 3, 4);
	}
?>
