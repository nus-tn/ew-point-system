<?php
/**
* 		
*/
require_once("includes/functions.php");
require_once("class/page.php");

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}


class project_profile extends page
{	
	var $pid;
	var $name;
	var $wing;
	var $msw;
	var $description;
	var $base_point;
	var $status;
	var $caller;

	public function __construct($pid, $caller) {
		$this->pid         = $pid;
		$this->name        = get_project_name($pid);
		$this->wing        = get_wing_name(get_project_wing($pid));
		$this->msw         = get_msw_name(get_project_msw($pid));
		$this->description = get_project_description($pid);
		$this->base_point  = get_project_base_point($pid);
		$this->status      = get_project_status($pid);
		$this->caller      = $caller;
	}
	
	public function display() {
		if (is_valid_id($this->pid, 'project')) {
			return $this->caller->display_menu() . $this->display_profile();
		} else {
			return $this->caller->display_menu() . $this->display_error();
		}
	}

	private function display_error() {
		return <<<INVALID_ID
			<p></p>
			<div style="text-align:center; color:#AAA">Invalid project id : {$this->pid}
			</div>
INVALID_ID;
	}

	private function display_profile() {
		$disabled    = 'disabled="disabled"';
		$manageable  = (is_admin($this->caller->uid) || is_head_of_project($this->caller->uid, $this->pid)) ? '' : $disabled;
		$submit	 	 = (is_admin($this->caller->uid) || is_head_of_project($this->caller->uid, $this->pid)) ? '' : 'style="display : none"';
		$placeholder = (is_admin($this->caller->uid) || is_head_of_project($this->caller->uid, $this->pid)) ? 'Enter comments here ...' : 'No comment yet ...';
		
		$title = 'Project Details';
		if (get_project_wing($this->pid) != WING_NULL) {
			$member = get_wing_member(get_project_wing($this->pid));
		} else {
			$member = get_msw_member(get_project_msw($this->pid));
		}
		$assignee = get_project_assignee($this->pid);
		$list = '';
		$assigned_list = '';
		foreach ($member as $key => $value) {
			if (in_array($value, $assignee)) {
				$rcid = get_record_id($this->pid, $value);
				// $list .= "<div class='checkbox'><input type='checkbox' name='member-$value' value='FALSE' checked disabled='disabled'/>" . get_user_name($value) . '</div>';
				$assigned_list .= "<div><a href='?operation=view-record&rcid=$rcid' />" . get_user_name($value) . '</div>';
			} else {
				$list .= "<div class='checkbox'><input type='checkbox' class='assign_checkbox' name='member-$value' value='TRUE' />" . get_user_name($value) . '</div>';
			}
		}
		if (get_project_status($this->pid) == PROJECT_STATUS_NOT_STARTED)
			$button = "<input type='submit' class='btn btn-default' $submit name='start-project' value='Start'>";
		else if(get_project_status($this->pid) == PROJECT_STATUS_IN_PROGRESS)
			$button = "<input type='submit' class='btn btn-default' $submit name='end-project' value='End'>";
		else
			$button = '';

		$project_name = get_project_name($this->pid);
		$project_description = get_project_description($this->pid);
		$project_base_point = get_project_base_point($this->pid);

		$output = <<<PROJECT_INFO
		<div>
			<h4 class="text-center">$title</h4>
		</div>
		<form name='profile-form' method='POST' id='profile-form'>
			<table class="table table-hover">
				<tr>
					<td>
						<label for='profile-name'>Name</label>
					</td>
					<td>
						<input type='text' class="form-control" name='profile-name' $manageable value='{$this->name}'/>
					</td>
				</tr>
				<tr>
					<td>
						<label>Specialization Wing</label>
					</td>
					<td>
						{$this->wing}
					</td>
				</tr>
				<tr>
					<td>
						<label>Media Support Wing</label>
					</td>
					<td>
						{$this->msw}
					</td>
				</tr>
				<tr>
					<td>
						<label for='profile-description'>Description</label>
					</td>
					<td>
						<input type='text' class="form-control" name='profile-description' $manageable value='{$this->description}'/>
					</td>
				</tr>
				<tr>
					<td>
						<label>Status</label>
					</td>
					<td>
						{$this->status}
					</td>
				</tr>
					<td>
						<label for='profile-point'>Base Point</label>
					</td>
					<td>
						<input type='text' class="form-control" name='profile-point' $manageable value='{$this->base_point}'/>
					</td>
				<tr $submit>
					<td>
						<label for='profile-assign'>Assign Project</label>
					</td>
					<td>
						$list
					</td>
				</tr>
				<!--<tr $submit>
					<td>
						<label for='profile-record'>Manage Records</label>
					</td>
					<td>
						$assigned_list
					</td>
				</tr>-->
PROJECT_INFO;

		$output .= <<<ADMIN_HOME_PROJECT
				<tr>
					<td>
						<label>Assigned records</label>
					</td>	
					<td style="padding:0">
						<table class="table table-plain" style="margin-bottom:0">
							<tr>
								<th>Name</th>
								<!--<th>Weightage</th>-->
								<th>Total</th>
								<th>Comments</th>
							</tr>
ADMIN_HOME_PROJECT;
		if (count($assignee) == 0) {
			$output .= <<<NO_ASSIGNEE_MESSAGE
				<tr>
					<td colspan=4>
						<div style="text-align:center; color:#AAA">This project has not been assigned to anyone yet.</div>
					</td>
				</tr>
NO_ASSIGNEE_MESSAGE;
		} else {
			foreach ($assignee as $id) {
				$rcid = get_record_id($this->pid, $id);

				$assignee_name      = get_user_name($id);
				// $assignee_weightage = get_record_weightage($rcid);
				$record_points      = get_record_points($rcid);
				$record_comments    = get_record_comments($rcid);

				// $selected = array();
				// for ($i = 0; $i <= 20; $i ++) { 
				// 	$selected[$i] = '';
				// }
				// $selected[$assignee_weightage * 10] = 'selected';
				$weightage_options = '';
				// for ($i = 0; $i <= 20; $i ++) { 
				// 	$wei = number_format($i / 10, 1);
				// 	$weightage_options .= "<option value='$wei'" . $selected[$i] . ">$wei</option>";
				// }

				$output .= <<<ADMIN_HOME_ASSIGNEE
					<tr>
						<td>$assignee_name</td>
						<!--<td>
							<select $manageable name='record-weightage-$rcid' class="form-control">
								$weightage_options
							</select>
						</td>-->
						<td>
							<input type='text' value=$record_points name='record-points-$rcid' class="form-control">
						</td>
						<td>
							<textarea $manageable class='form-control' name='record-comments-$rcid' placeholder='$placeholder' >$record_comments</textarea>
							</td>
					</tr>
ADMIN_HOME_ASSIGNEE;
			}
		}
		$output .= <<<CLOSING
		</table>
		</td>
		</tr>
		<tr>
			<td style="text-align: center" colspan=2>
				<input type='submit' class="btn btn-default" $submit name='update-project' value='Update'>
				$button
				<input type='submit' class="btn btn-danger" $submit name='delete-project' value='Delete'>
			</td>
		</tr>
		</table>
CLOSING;


		return $output;
	}
}
?>