<?php
/**
* 		
*/
require_once('includes/db.php');
require_once('includes/constants.inc');
require_once('class/user.php');
require_once('class/head.php');
require_once('class/member_profile.php');
require_once('class/project_profile.php');
require_once('class/record_profile.php');

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

class admin extends user
{	

	function __construct($uid) {
		parent :: __construct($uid);
	}

	public function display() {
		if (is_admin($this->uid)) {
			$photo = ADMIN_ROLE_PHOTO_HEAD;
			$video = ADMIN_ROLE_VIDEO_HEAD;
			$it = ADMIN_ROLE_IT_HEAD;
			$design = ADMIN_ROLE_DESIGN_HEAD;
			$culture = ADMIN_ROLE_CULTURE_HEAD;
			$dp = ADMIN_ROLE_DP_HEAD;
			$ihg = ADMIN_ROLE_IHG_HEAD;
			$output = <<<ADMIN_HOME_HEADER
				<br />
				<div>
					<table class="table table-plain" style="text-align : center">
					<tr><td colspan = '4'><h4 class="text-center">masquerade</h4></td></tr>
					<tr>
						<td><a href="?switch_role={$photo}">Photo wing head</a></td>
						<td><a href="?switch_role={$video}">Video wing head</a></td>
						<td><a href="?switch_role={$it}">IT wing head</a></td>
						<td><a href="?switch_role={$design}">Design wing head</a></td>
					</tr>
					<tr>
						<td><a href="?switch_role={$culture}">Culture msw head</a></td>
						<td><a href="?switch_role={$dp}">DP msw head</a></td>
						<td><a href="?switch_role={$ihg}">IHG msw head</a></td>
						<td><a href="#" class="disabled">I just wanna see everything</a></td>
					</tr>
				</div>
ADMIN_HOME_HEADER;

			return $output;
		}
		

		return $this->uid;
	}

	
}

?>